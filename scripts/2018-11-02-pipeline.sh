#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#-------------------------------------------------------------------------------
# RnaSeq SlurmScheduler Job Submission Bash script
# Version: 3.0.1-beta
# Created on: 2018-11-03T03:49:14
# Steps:
#   picard_sam_to_fastq: 0 job... skipping
#   trimmomatic: 0 job... skipping
#   merge_trimmomatic_stats: 0 job... skipping
#   star: 0 job... skipping
#   picard_merge_sam_files: 0 job... skipping
#   picard_sort_sam: 0 job... skipping
#   picard_mark_duplicates: 0 job... skipping
#   picard_rna_metrics: 48 jobs
#   estimate_ribosomal_rna: 48 jobs
#   bam_hard_clip: 0 job... skipping
#   rnaseqc: 2 jobs
#   wiggle: 96 jobs
#   raw_counts: 0 job... skipping
#   raw_counts_metrics: 3 jobs
#   cufflinks: 0 job... skipping
#   cuffmerge: 0 job... skipping
#   cuffquant: 0 job... skipping
#   cuffdiff: 9 jobs
#   cuffnorm: 0 job... skipping
#   fpkm_correlation_matrix: 0 job... skipping
#   TOTAL: 206 jobs
#-------------------------------------------------------------------------------

OUTPUT_DIR=/scratch/efournie/YM_rnaseq/output/pipeline
JOB_OUTPUT_DIR=$OUTPUT_DIR/job_output
TIMESTAMP=`date +%FT%H.%M.%S`
JOB_LIST=$JOB_OUTPUT_DIR/RnaSeq_job_list_$TIMESTAMP
export CONFIG_FILES="/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini"
mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR


#-------------------------------------------------------------------------------
# STEP: picard_rna_metrics
#-------------------------------------------------------------------------------
STEP=picard_rna_metrics
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_1_JOB_ID: picard_rna_metrics.Tx1_Days0_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days0_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days0_Rep1.eb0a4a2636b1d277e0393a9bae3d5882.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days0_Rep1.eb0a4a2636b1d277e0393a9bae3d5882.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days0_Rep1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days0_Rep1/Tx1_Days0_Rep1.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days0_Rep1/Tx1_Days0_Rep1 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days0_Rep1/Tx1_Days0_Rep1.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days0_Rep1/Tx1_Days0_Rep1.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days0_Rep1.eb0a4a2636b1d277e0393a9bae3d5882.mugqic.done
)
picard_rna_metrics_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_2_JOB_ID: picard_rna_metrics.Tx1_Days0_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days0_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days0_Rep2.324103fd11bd380d56fffa8407245bea.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days0_Rep2.324103fd11bd380d56fffa8407245bea.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days0_Rep2 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days0_Rep2/Tx1_Days0_Rep2.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days0_Rep2/Tx1_Days0_Rep2 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days0_Rep2/Tx1_Days0_Rep2.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days0_Rep2/Tx1_Days0_Rep2.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days0_Rep2.324103fd11bd380d56fffa8407245bea.mugqic.done
)
picard_rna_metrics_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_3_JOB_ID: picard_rna_metrics.Tx1_Days0_Rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days0_Rep3
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days0_Rep3.8e34313a9690b5697654cba89c078613.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days0_Rep3.8e34313a9690b5697654cba89c078613.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days0_Rep3 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days0_Rep3/Tx1_Days0_Rep3.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days0_Rep3/Tx1_Days0_Rep3 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days0_Rep3/Tx1_Days0_Rep3.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days0_Rep3/Tx1_Days0_Rep3.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days0_Rep3.8e34313a9690b5697654cba89c078613.mugqic.done
)
picard_rna_metrics_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_4_JOB_ID: picard_rna_metrics.Tx1_Days0_Rep4
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days0_Rep4
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days0_Rep4.c1b29586da13b385e96ef27ebac42e65.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days0_Rep4.c1b29586da13b385e96ef27ebac42e65.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days0_Rep4 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days0_Rep4/Tx1_Days0_Rep4.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days0_Rep4/Tx1_Days0_Rep4 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days0_Rep4/Tx1_Days0_Rep4.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days0_Rep4/Tx1_Days0_Rep4.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days0_Rep4.c1b29586da13b385e96ef27ebac42e65.mugqic.done
)
picard_rna_metrics_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_5_JOB_ID: picard_rna_metrics.Tx1_Days0_Rep5
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days0_Rep5
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days0_Rep5.61ff842e9dd5b27823504c92790e4c51.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days0_Rep5.61ff842e9dd5b27823504c92790e4c51.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days0_Rep5 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days0_Rep5/Tx1_Days0_Rep5.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days0_Rep5/Tx1_Days0_Rep5 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days0_Rep5/Tx1_Days0_Rep5.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days0_Rep5/Tx1_Days0_Rep5.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days0_Rep5.61ff842e9dd5b27823504c92790e4c51.mugqic.done
)
picard_rna_metrics_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_6_JOB_ID: picard_rna_metrics.Tx1_Days0_Rep6
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days0_Rep6
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days0_Rep6.eb5767fead1768dde89cab32780d16d2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days0_Rep6.eb5767fead1768dde89cab32780d16d2.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days0_Rep6 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days0_Rep6/Tx1_Days0_Rep6.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days0_Rep6/Tx1_Days0_Rep6 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days0_Rep6/Tx1_Days0_Rep6.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days0_Rep6/Tx1_Days0_Rep6.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days0_Rep6.eb5767fead1768dde89cab32780d16d2.mugqic.done
)
picard_rna_metrics_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_7_JOB_ID: picard_rna_metrics.Tx1_Days0_Rep7
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days0_Rep7
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days0_Rep7.89a73d8f5cd11e024f7df7ca78fa7bf5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days0_Rep7.89a73d8f5cd11e024f7df7ca78fa7bf5.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days0_Rep7 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days0_Rep7/Tx1_Days0_Rep7.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days0_Rep7/Tx1_Days0_Rep7 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days0_Rep7/Tx1_Days0_Rep7.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days0_Rep7/Tx1_Days0_Rep7.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days0_Rep7.89a73d8f5cd11e024f7df7ca78fa7bf5.mugqic.done
)
picard_rna_metrics_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_8_JOB_ID: picard_rna_metrics.Tx1_Days0_Rep8
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days0_Rep8
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days0_Rep8.a1e808e228d62027815647af9524845e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days0_Rep8.a1e808e228d62027815647af9524845e.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days0_Rep8 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days0_Rep8/Tx1_Days0_Rep8.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days0_Rep8/Tx1_Days0_Rep8 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days0_Rep8/Tx1_Days0_Rep8.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days0_Rep8/Tx1_Days0_Rep8.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days0_Rep8.a1e808e228d62027815647af9524845e.mugqic.done
)
picard_rna_metrics_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_9_JOB_ID: picard_rna_metrics.Tx1_Days2_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days2_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days2_Rep1.58c02462c340462f004b2df1fa3156e8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days2_Rep1.58c02462c340462f004b2df1fa3156e8.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days2_Rep1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days2_Rep1/Tx1_Days2_Rep1.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days2_Rep1/Tx1_Days2_Rep1 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days2_Rep1/Tx1_Days2_Rep1.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days2_Rep1/Tx1_Days2_Rep1.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days2_Rep1.58c02462c340462f004b2df1fa3156e8.mugqic.done
)
picard_rna_metrics_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_10_JOB_ID: picard_rna_metrics.Tx1_Days2_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days2_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days2_Rep2.0a0417cd0478fc109a737458b9bf2f52.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days2_Rep2.0a0417cd0478fc109a737458b9bf2f52.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days2_Rep2 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days2_Rep2/Tx1_Days2_Rep2.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days2_Rep2/Tx1_Days2_Rep2 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days2_Rep2/Tx1_Days2_Rep2.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days2_Rep2/Tx1_Days2_Rep2.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days2_Rep2.0a0417cd0478fc109a737458b9bf2f52.mugqic.done
)
picard_rna_metrics_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_11_JOB_ID: picard_rna_metrics.Tx1_Days2_Rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days2_Rep3
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days2_Rep3.86264b2a430ba8ee88e063c732f2aabe.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days2_Rep3.86264b2a430ba8ee88e063c732f2aabe.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days2_Rep3 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days2_Rep3/Tx1_Days2_Rep3.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days2_Rep3/Tx1_Days2_Rep3 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days2_Rep3/Tx1_Days2_Rep3.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days2_Rep3/Tx1_Days2_Rep3.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days2_Rep3.86264b2a430ba8ee88e063c732f2aabe.mugqic.done
)
picard_rna_metrics_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_12_JOB_ID: picard_rna_metrics.Tx1_Days2_Rep4
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days2_Rep4
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days2_Rep4.1e83b849e34e7b6639102c2510eb84fd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days2_Rep4.1e83b849e34e7b6639102c2510eb84fd.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days2_Rep4 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days2_Rep4/Tx1_Days2_Rep4.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days2_Rep4/Tx1_Days2_Rep4 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days2_Rep4/Tx1_Days2_Rep4.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days2_Rep4/Tx1_Days2_Rep4.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days2_Rep4.1e83b849e34e7b6639102c2510eb84fd.mugqic.done
)
picard_rna_metrics_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_13_JOB_ID: picard_rna_metrics.Tx1_Days2_Rep5
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days2_Rep5
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days2_Rep5.5912c56fd3d312d58c00c18dafc1ab08.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days2_Rep5.5912c56fd3d312d58c00c18dafc1ab08.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days2_Rep5 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days2_Rep5/Tx1_Days2_Rep5.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days2_Rep5/Tx1_Days2_Rep5 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days2_Rep5/Tx1_Days2_Rep5.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days2_Rep5/Tx1_Days2_Rep5.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days2_Rep5.5912c56fd3d312d58c00c18dafc1ab08.mugqic.done
)
picard_rna_metrics_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_14_JOB_ID: picard_rna_metrics.Tx1_Days2_Rep6
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days2_Rep6
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days2_Rep6.f970857a8442e3cc05a2c88f7efb4ce1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days2_Rep6.f970857a8442e3cc05a2c88f7efb4ce1.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days2_Rep6 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days2_Rep6/Tx1_Days2_Rep6.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days2_Rep6/Tx1_Days2_Rep6 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days2_Rep6/Tx1_Days2_Rep6.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days2_Rep6/Tx1_Days2_Rep6.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days2_Rep6.f970857a8442e3cc05a2c88f7efb4ce1.mugqic.done
)
picard_rna_metrics_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_15_JOB_ID: picard_rna_metrics.Tx1_Days2_Rep7
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days2_Rep7
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days2_Rep7.bb3e499a71153f1cdbbc99cc11a75a63.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days2_Rep7.bb3e499a71153f1cdbbc99cc11a75a63.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days2_Rep7 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days2_Rep7/Tx1_Days2_Rep7.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days2_Rep7/Tx1_Days2_Rep7 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days2_Rep7/Tx1_Days2_Rep7.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days2_Rep7/Tx1_Days2_Rep7.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days2_Rep7.bb3e499a71153f1cdbbc99cc11a75a63.mugqic.done
)
picard_rna_metrics_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_16_JOB_ID: picard_rna_metrics.Tx1_Days2_Rep8
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days2_Rep8
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days2_Rep8.13718d060bf68734b6139194ac6c97ba.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days2_Rep8.13718d060bf68734b6139194ac6c97ba.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days2_Rep8 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days2_Rep8/Tx1_Days2_Rep8.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days2_Rep8/Tx1_Days2_Rep8 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days2_Rep8/Tx1_Days2_Rep8.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days2_Rep8/Tx1_Days2_Rep8.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days2_Rep8.13718d060bf68734b6139194ac6c97ba.mugqic.done
)
picard_rna_metrics_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_17_JOB_ID: picard_rna_metrics.Tx1_Days8_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days8_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days8_Rep1.300e0f40e215a67f21f4c0c9457d6945.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days8_Rep1.300e0f40e215a67f21f4c0c9457d6945.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days8_Rep1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days8_Rep1/Tx1_Days8_Rep1.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days8_Rep1/Tx1_Days8_Rep1 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days8_Rep1/Tx1_Days8_Rep1.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days8_Rep1/Tx1_Days8_Rep1.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days8_Rep1.300e0f40e215a67f21f4c0c9457d6945.mugqic.done
)
picard_rna_metrics_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_18_JOB_ID: picard_rna_metrics.Tx1_Days8_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days8_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days8_Rep2.763db3cdf8f54ed8d670a5b79425753e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days8_Rep2.763db3cdf8f54ed8d670a5b79425753e.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days8_Rep2 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days8_Rep2/Tx1_Days8_Rep2.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days8_Rep2/Tx1_Days8_Rep2 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days8_Rep2/Tx1_Days8_Rep2.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days8_Rep2/Tx1_Days8_Rep2.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days8_Rep2.763db3cdf8f54ed8d670a5b79425753e.mugqic.done
)
picard_rna_metrics_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_19_JOB_ID: picard_rna_metrics.Tx1_Days8_Rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days8_Rep3
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days8_Rep3.b76b9246bc3bf368b3aff3f1489b9297.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days8_Rep3.b76b9246bc3bf368b3aff3f1489b9297.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days8_Rep3 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days8_Rep3/Tx1_Days8_Rep3.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days8_Rep3/Tx1_Days8_Rep3 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days8_Rep3/Tx1_Days8_Rep3.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days8_Rep3/Tx1_Days8_Rep3.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days8_Rep3.b76b9246bc3bf368b3aff3f1489b9297.mugqic.done
)
picard_rna_metrics_19_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_20_JOB_ID: picard_rna_metrics.Tx1_Days8_Rep4
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days8_Rep4
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days8_Rep4.20e28c5a06a904d13599819760a52bae.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days8_Rep4.20e28c5a06a904d13599819760a52bae.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days8_Rep4 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days8_Rep4/Tx1_Days8_Rep4.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days8_Rep4/Tx1_Days8_Rep4 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days8_Rep4/Tx1_Days8_Rep4.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days8_Rep4/Tx1_Days8_Rep4.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days8_Rep4.20e28c5a06a904d13599819760a52bae.mugqic.done
)
picard_rna_metrics_20_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_21_JOB_ID: picard_rna_metrics.Tx1_Days8_Rep5
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days8_Rep5
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days8_Rep5.31afb7440087a58daec535bf5c148187.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days8_Rep5.31afb7440087a58daec535bf5c148187.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days8_Rep5 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days8_Rep5/Tx1_Days8_Rep5.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days8_Rep5/Tx1_Days8_Rep5 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days8_Rep5/Tx1_Days8_Rep5.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days8_Rep5/Tx1_Days8_Rep5.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days8_Rep5.31afb7440087a58daec535bf5c148187.mugqic.done
)
picard_rna_metrics_21_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_22_JOB_ID: picard_rna_metrics.Tx1_Days8_Rep6
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days8_Rep6
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days8_Rep6.4cd0142dd982ffc6c2fcdda0eaa0cec7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days8_Rep6.4cd0142dd982ffc6c2fcdda0eaa0cec7.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days8_Rep6 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days8_Rep6/Tx1_Days8_Rep6.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days8_Rep6/Tx1_Days8_Rep6 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days8_Rep6/Tx1_Days8_Rep6.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days8_Rep6/Tx1_Days8_Rep6.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days8_Rep6.4cd0142dd982ffc6c2fcdda0eaa0cec7.mugqic.done
)
picard_rna_metrics_22_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_23_JOB_ID: picard_rna_metrics.Tx1_Days8_Rep7
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days8_Rep7
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days8_Rep7.f6906503a35b9e32432f743a0b3b3b03.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days8_Rep7.f6906503a35b9e32432f743a0b3b3b03.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days8_Rep7 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days8_Rep7/Tx1_Days8_Rep7.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days8_Rep7/Tx1_Days8_Rep7 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days8_Rep7/Tx1_Days8_Rep7.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days8_Rep7/Tx1_Days8_Rep7.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days8_Rep7.f6906503a35b9e32432f743a0b3b3b03.mugqic.done
)
picard_rna_metrics_23_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_24_JOB_ID: picard_rna_metrics.Tx1_Days8_Rep8
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx1_Days8_Rep8
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx1_Days8_Rep8.dc5901929245310a998d46081d313bf6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx1_Days8_Rep8.dc5901929245310a998d46081d313bf6.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx1_Days8_Rep8 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx1_Days8_Rep8/Tx1_Days8_Rep8.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days8_Rep8/Tx1_Days8_Rep8 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx1_Days8_Rep8/Tx1_Days8_Rep8.sorted.mdup.bam \
 OUTPUT=metrics/Tx1_Days8_Rep8/Tx1_Days8_Rep8.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx1_Days8_Rep8.dc5901929245310a998d46081d313bf6.mugqic.done
)
picard_rna_metrics_24_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_25_JOB_ID: picard_rna_metrics.Tx3_Days0_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days0_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days0_Rep1.be694a09c56f54711c25af0e96cca87c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days0_Rep1.be694a09c56f54711c25af0e96cca87c.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days0_Rep1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days0_Rep1/Tx3_Days0_Rep1.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days0_Rep1/Tx3_Days0_Rep1 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days0_Rep1/Tx3_Days0_Rep1.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days0_Rep1/Tx3_Days0_Rep1.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days0_Rep1.be694a09c56f54711c25af0e96cca87c.mugqic.done
)
picard_rna_metrics_25_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_25_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_26_JOB_ID: picard_rna_metrics.Tx3_Days0_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days0_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days0_Rep2.6d88733850722797923172ef74edd60a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days0_Rep2.6d88733850722797923172ef74edd60a.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days0_Rep2 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days0_Rep2/Tx3_Days0_Rep2.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days0_Rep2/Tx3_Days0_Rep2 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days0_Rep2/Tx3_Days0_Rep2.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days0_Rep2/Tx3_Days0_Rep2.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days0_Rep2.6d88733850722797923172ef74edd60a.mugqic.done
)
picard_rna_metrics_26_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_26_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_27_JOB_ID: picard_rna_metrics.Tx3_Days0_Rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days0_Rep3
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days0_Rep3.86d0348108a90442f0e66ec70c735a2d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days0_Rep3.86d0348108a90442f0e66ec70c735a2d.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days0_Rep3 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days0_Rep3/Tx3_Days0_Rep3.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days0_Rep3/Tx3_Days0_Rep3 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days0_Rep3/Tx3_Days0_Rep3.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days0_Rep3/Tx3_Days0_Rep3.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days0_Rep3.86d0348108a90442f0e66ec70c735a2d.mugqic.done
)
picard_rna_metrics_27_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_27_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_28_JOB_ID: picard_rna_metrics.Tx3_Days0_Rep4
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days0_Rep4
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days0_Rep4.02b2b6e23ff182173184b25abf233159.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days0_Rep4.02b2b6e23ff182173184b25abf233159.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days0_Rep4 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days0_Rep4/Tx3_Days0_Rep4.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days0_Rep4/Tx3_Days0_Rep4 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days0_Rep4/Tx3_Days0_Rep4.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days0_Rep4/Tx3_Days0_Rep4.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days0_Rep4.02b2b6e23ff182173184b25abf233159.mugqic.done
)
picard_rna_metrics_28_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_28_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_29_JOB_ID: picard_rna_metrics.Tx3_Days0_Rep5
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days0_Rep5
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days0_Rep5.87c3303c6433a4bae6456795a832487b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days0_Rep5.87c3303c6433a4bae6456795a832487b.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days0_Rep5 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days0_Rep5/Tx3_Days0_Rep5.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days0_Rep5/Tx3_Days0_Rep5 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days0_Rep5/Tx3_Days0_Rep5.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days0_Rep5/Tx3_Days0_Rep5.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days0_Rep5.87c3303c6433a4bae6456795a832487b.mugqic.done
)
picard_rna_metrics_29_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_29_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_30_JOB_ID: picard_rna_metrics.Tx3_Days0_Rep6
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days0_Rep6
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days0_Rep6.b765e49936312fc24f5be99ea5b15316.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days0_Rep6.b765e49936312fc24f5be99ea5b15316.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days0_Rep6 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days0_Rep6/Tx3_Days0_Rep6.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days0_Rep6/Tx3_Days0_Rep6 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days0_Rep6/Tx3_Days0_Rep6.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days0_Rep6/Tx3_Days0_Rep6.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days0_Rep6.b765e49936312fc24f5be99ea5b15316.mugqic.done
)
picard_rna_metrics_30_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_30_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_31_JOB_ID: picard_rna_metrics.Tx3_Days0_Rep7
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days0_Rep7
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days0_Rep7.03693ca127d1c6abe5864ccac989134e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days0_Rep7.03693ca127d1c6abe5864ccac989134e.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days0_Rep7 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days0_Rep7/Tx3_Days0_Rep7.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days0_Rep7/Tx3_Days0_Rep7 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days0_Rep7/Tx3_Days0_Rep7.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days0_Rep7/Tx3_Days0_Rep7.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days0_Rep7.03693ca127d1c6abe5864ccac989134e.mugqic.done
)
picard_rna_metrics_31_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_31_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_32_JOB_ID: picard_rna_metrics.Tx3_Days0_Rep8
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days0_Rep8
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days0_Rep8.31e69feb73bf3c894d3361aba15eaf34.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days0_Rep8.31e69feb73bf3c894d3361aba15eaf34.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days0_Rep8 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days0_Rep8/Tx3_Days0_Rep8.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days0_Rep8/Tx3_Days0_Rep8 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days0_Rep8/Tx3_Days0_Rep8.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days0_Rep8/Tx3_Days0_Rep8.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days0_Rep8.31e69feb73bf3c894d3361aba15eaf34.mugqic.done
)
picard_rna_metrics_32_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_32_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_33_JOB_ID: picard_rna_metrics.Tx3_Days2_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days2_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days2_Rep1.03b6caf78761cdf99571edb212217e38.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days2_Rep1.03b6caf78761cdf99571edb212217e38.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days2_Rep1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days2_Rep1/Tx3_Days2_Rep1.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days2_Rep1/Tx3_Days2_Rep1 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days2_Rep1/Tx3_Days2_Rep1.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days2_Rep1/Tx3_Days2_Rep1.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days2_Rep1.03b6caf78761cdf99571edb212217e38.mugqic.done
)
picard_rna_metrics_33_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_33_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_34_JOB_ID: picard_rna_metrics.Tx3_Days2_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days2_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days2_Rep2.abe040abb00019a27dca1bfa91c868b1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days2_Rep2.abe040abb00019a27dca1bfa91c868b1.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days2_Rep2 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days2_Rep2/Tx3_Days2_Rep2.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days2_Rep2/Tx3_Days2_Rep2 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days2_Rep2/Tx3_Days2_Rep2.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days2_Rep2/Tx3_Days2_Rep2.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days2_Rep2.abe040abb00019a27dca1bfa91c868b1.mugqic.done
)
picard_rna_metrics_34_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_34_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_35_JOB_ID: picard_rna_metrics.Tx3_Days2_Rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days2_Rep3
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days2_Rep3.2e990a9ff50d4496ffa1906b489a7067.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days2_Rep3.2e990a9ff50d4496ffa1906b489a7067.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days2_Rep3 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days2_Rep3/Tx3_Days2_Rep3.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days2_Rep3/Tx3_Days2_Rep3 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days2_Rep3/Tx3_Days2_Rep3.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days2_Rep3/Tx3_Days2_Rep3.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days2_Rep3.2e990a9ff50d4496ffa1906b489a7067.mugqic.done
)
picard_rna_metrics_35_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_35_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_36_JOB_ID: picard_rna_metrics.Tx3_Days2_Rep4
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days2_Rep4
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days2_Rep4.b02ea6b19b09ebd613d630a7f215f6b2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days2_Rep4.b02ea6b19b09ebd613d630a7f215f6b2.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days2_Rep4 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days2_Rep4/Tx3_Days2_Rep4.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days2_Rep4/Tx3_Days2_Rep4 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days2_Rep4/Tx3_Days2_Rep4.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days2_Rep4/Tx3_Days2_Rep4.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days2_Rep4.b02ea6b19b09ebd613d630a7f215f6b2.mugqic.done
)
picard_rna_metrics_36_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_36_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_37_JOB_ID: picard_rna_metrics.Tx3_Days2_Rep5
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days2_Rep5
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days2_Rep5.67c6c357ecec81fea372912e52708f51.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days2_Rep5.67c6c357ecec81fea372912e52708f51.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days2_Rep5 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days2_Rep5/Tx3_Days2_Rep5.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days2_Rep5/Tx3_Days2_Rep5 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days2_Rep5/Tx3_Days2_Rep5.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days2_Rep5/Tx3_Days2_Rep5.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days2_Rep5.67c6c357ecec81fea372912e52708f51.mugqic.done
)
picard_rna_metrics_37_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_37_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_38_JOB_ID: picard_rna_metrics.Tx3_Days2_Rep6
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days2_Rep6
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days2_Rep6.09aeb16929da9a204236e25d690e7409.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days2_Rep6.09aeb16929da9a204236e25d690e7409.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days2_Rep6 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days2_Rep6/Tx3_Days2_Rep6.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days2_Rep6/Tx3_Days2_Rep6 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days2_Rep6/Tx3_Days2_Rep6.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days2_Rep6/Tx3_Days2_Rep6.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days2_Rep6.09aeb16929da9a204236e25d690e7409.mugqic.done
)
picard_rna_metrics_38_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_38_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_39_JOB_ID: picard_rna_metrics.Tx3_Days2_Rep7
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days2_Rep7
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days2_Rep7.b21f590a9d69d1aa9042d9a16914b2bc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days2_Rep7.b21f590a9d69d1aa9042d9a16914b2bc.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days2_Rep7 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days2_Rep7/Tx3_Days2_Rep7.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days2_Rep7/Tx3_Days2_Rep7 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days2_Rep7/Tx3_Days2_Rep7.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days2_Rep7/Tx3_Days2_Rep7.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days2_Rep7.b21f590a9d69d1aa9042d9a16914b2bc.mugqic.done
)
picard_rna_metrics_39_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_39_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_40_JOB_ID: picard_rna_metrics.Tx3_Days2_Rep8
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days2_Rep8
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days2_Rep8.617d309f96e6e06942ced369388b495b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days2_Rep8.617d309f96e6e06942ced369388b495b.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days2_Rep8 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days2_Rep8/Tx3_Days2_Rep8.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days2_Rep8/Tx3_Days2_Rep8 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days2_Rep8/Tx3_Days2_Rep8.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days2_Rep8/Tx3_Days2_Rep8.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days2_Rep8.617d309f96e6e06942ced369388b495b.mugqic.done
)
picard_rna_metrics_40_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_40_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_41_JOB_ID: picard_rna_metrics.Tx3_Days8_Rep1
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days8_Rep1
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days8_Rep1.cd466018f4259281fb2ea74ffd5a0109.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days8_Rep1.cd466018f4259281fb2ea74ffd5a0109.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days8_Rep1 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days8_Rep1/Tx3_Days8_Rep1.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days8_Rep1/Tx3_Days8_Rep1 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days8_Rep1/Tx3_Days8_Rep1.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days8_Rep1/Tx3_Days8_Rep1.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days8_Rep1.cd466018f4259281fb2ea74ffd5a0109.mugqic.done
)
picard_rna_metrics_41_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_41_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_42_JOB_ID: picard_rna_metrics.Tx3_Days8_Rep2
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days8_Rep2
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days8_Rep2.7b39c2700303f14efed19de8ca6dc343.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days8_Rep2.7b39c2700303f14efed19de8ca6dc343.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days8_Rep2 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days8_Rep2/Tx3_Days8_Rep2.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days8_Rep2/Tx3_Days8_Rep2 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days8_Rep2/Tx3_Days8_Rep2.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days8_Rep2/Tx3_Days8_Rep2.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days8_Rep2.7b39c2700303f14efed19de8ca6dc343.mugqic.done
)
picard_rna_metrics_42_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_42_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_43_JOB_ID: picard_rna_metrics.Tx3_Days8_Rep3
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days8_Rep3
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days8_Rep3.667270ff02bfe194ba16053d5e798cbd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days8_Rep3.667270ff02bfe194ba16053d5e798cbd.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days8_Rep3 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days8_Rep3/Tx3_Days8_Rep3.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days8_Rep3/Tx3_Days8_Rep3 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days8_Rep3/Tx3_Days8_Rep3.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days8_Rep3/Tx3_Days8_Rep3.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days8_Rep3.667270ff02bfe194ba16053d5e798cbd.mugqic.done
)
picard_rna_metrics_43_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_43_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_44_JOB_ID: picard_rna_metrics.Tx3_Days8_Rep4
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days8_Rep4
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days8_Rep4.18a32800c32c3d573a2f875e82d967fd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days8_Rep4.18a32800c32c3d573a2f875e82d967fd.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days8_Rep4 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days8_Rep4/Tx3_Days8_Rep4.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days8_Rep4/Tx3_Days8_Rep4 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days8_Rep4/Tx3_Days8_Rep4.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days8_Rep4/Tx3_Days8_Rep4.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days8_Rep4.18a32800c32c3d573a2f875e82d967fd.mugqic.done
)
picard_rna_metrics_44_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_44_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_45_JOB_ID: picard_rna_metrics.Tx3_Days8_Rep5
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days8_Rep5
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days8_Rep5.5ad346aa2d7af00d274aa4e323a8ca5b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days8_Rep5.5ad346aa2d7af00d274aa4e323a8ca5b.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days8_Rep5 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days8_Rep5/Tx3_Days8_Rep5.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days8_Rep5/Tx3_Days8_Rep5 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days8_Rep5/Tx3_Days8_Rep5.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days8_Rep5/Tx3_Days8_Rep5.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days8_Rep5.5ad346aa2d7af00d274aa4e323a8ca5b.mugqic.done
)
picard_rna_metrics_45_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_45_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_46_JOB_ID: picard_rna_metrics.Tx3_Days8_Rep6
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days8_Rep6
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days8_Rep6.7cd09f713ce1323a96923c5693c4a470.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days8_Rep6.7cd09f713ce1323a96923c5693c4a470.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days8_Rep6 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days8_Rep6/Tx3_Days8_Rep6.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days8_Rep6/Tx3_Days8_Rep6 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days8_Rep6/Tx3_Days8_Rep6.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days8_Rep6/Tx3_Days8_Rep6.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days8_Rep6.7cd09f713ce1323a96923c5693c4a470.mugqic.done
)
picard_rna_metrics_46_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_46_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_47_JOB_ID: picard_rna_metrics.Tx3_Days8_Rep7
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days8_Rep7
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days8_Rep7.90feaaaade8b6dfbd50c58435280651e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days8_Rep7.90feaaaade8b6dfbd50c58435280651e.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days8_Rep7 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days8_Rep7/Tx3_Days8_Rep7.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days8_Rep7/Tx3_Days8_Rep7 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days8_Rep7/Tx3_Days8_Rep7.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days8_Rep7/Tx3_Days8_Rep7.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days8_Rep7.90feaaaade8b6dfbd50c58435280651e.mugqic.done
)
picard_rna_metrics_47_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_47_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: picard_rna_metrics_48_JOB_ID: picard_rna_metrics.Tx3_Days8_Rep8
#-------------------------------------------------------------------------------
JOB_NAME=picard_rna_metrics.Tx3_Days8_Rep8
JOB_DEPENDENCIES=
JOB_DONE=job_output/picard_rna_metrics/picard_rna_metrics.Tx3_Days8_Rep8.f72718d9625845900f63ce2558fe36f9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_rna_metrics.Tx3_Days8_Rep8.f72718d9625845900f63ce2558fe36f9.mugqic.done'
module load java/1.8.0_121 mugqic/picard/2.9.0 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p metrics/Tx3_Days8_Rep8 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectMultipleMetrics \
 PROGRAM=CollectAlignmentSummaryMetrics PROGRAM=CollectInsertSizeMetrics VALIDATION_STRINGENCY=SILENT \
 TMP_DIR=${SLURM_TMPDIR} \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 INPUT=alignment/Tx3_Days8_Rep8/Tx3_Days8_Rep8.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days8_Rep8/Tx3_Days8_Rep8 \
 MAX_RECORDS_IN_RAM=5750000 && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $PICARD_HOME/picard.jar CollectRnaSeqMetrics \
 VALIDATION_STRINGENCY=SILENT  \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=alignment/Tx3_Days8_Rep8/Tx3_Days8_Rep8.sorted.mdup.bam \
 OUTPUT=metrics/Tx3_Days8_Rep8/Tx3_Days8_Rep8.picard_rna_metrics \
 REF_FLAT=/scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.ref_flat.tsv \
 STRAND_SPECIFICITY=SECOND_READ_TRANSCRIPTION_STRAND \
 MINIMUM_LENGTH=200 \
 REFERENCE_SEQUENCE=/scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
 MAX_RECORDS_IN_RAM=5750000
picard_rna_metrics.Tx3_Days8_Rep8.f72718d9625845900f63ce2558fe36f9.mugqic.done
)
picard_rna_metrics_48_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"picard_rna_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$picard_rna_metrics_48_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: estimate_ribosomal_rna
#-------------------------------------------------------------------------------
STEP=estimate_ribosomal_rna
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_1_JOB_ID: bwa_mem_rRNA.Tx1_Days0_Rep1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days0_Rep1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days0_Rep1_RS.ece5ae6b4234642366db512d774175ff.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days0_Rep1_RS.ece5ae6b4234642366db512d774175ff.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days0_Rep1/Tx1_Days0_Rep1_RS metrics/Tx1_Days0_Rep1/Tx1_Days0_Rep1_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days0_Rep1/Tx1_Days0_Rep1_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days0_Rep1_RS	SM:Tx1_Days0_Rep1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days0_Rep1/Tx1_Days0_Rep1_RS/Tx1_Days0_Rep1_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days0_Rep1/Tx1_Days0_Rep1_RS/Tx1_Days0_Rep1_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days0_Rep1/Tx1_Days0_Rep1_RS/Tx1_Days0_Rep1_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days0_Rep1_RS.ece5ae6b4234642366db512d774175ff.mugqic.done
)
estimate_ribosomal_rna_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_2_JOB_ID: bwa_mem_rRNA.Tx1_Days0_Rep2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days0_Rep2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days0_Rep2_RS.cf22dd993dec3302599c784a70206ff8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days0_Rep2_RS.cf22dd993dec3302599c784a70206ff8.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days0_Rep2/Tx1_Days0_Rep2_RS metrics/Tx1_Days0_Rep2/Tx1_Days0_Rep2_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days0_Rep2/Tx1_Days0_Rep2_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days0_Rep2_RS	SM:Tx1_Days0_Rep2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days0_Rep2/Tx1_Days0_Rep2_RS/Tx1_Days0_Rep2_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days0_Rep2/Tx1_Days0_Rep2_RS/Tx1_Days0_Rep2_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days0_Rep2/Tx1_Days0_Rep2_RS/Tx1_Days0_Rep2_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days0_Rep2_RS.cf22dd993dec3302599c784a70206ff8.mugqic.done
)
estimate_ribosomal_rna_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_3_JOB_ID: bwa_mem_rRNA.Tx1_Days0_Rep3_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days0_Rep3_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days0_Rep3_RS.e6693a18f500754351e42772eccb79a9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days0_Rep3_RS.e6693a18f500754351e42772eccb79a9.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days0_Rep3/Tx1_Days0_Rep3_RS metrics/Tx1_Days0_Rep3/Tx1_Days0_Rep3_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days0_Rep3/Tx1_Days0_Rep3_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days0_Rep3_RS	SM:Tx1_Days0_Rep3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days0_Rep3/Tx1_Days0_Rep3_RS/Tx1_Days0_Rep3_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days0_Rep3/Tx1_Days0_Rep3_RS/Tx1_Days0_Rep3_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days0_Rep3/Tx1_Days0_Rep3_RS/Tx1_Days0_Rep3_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days0_Rep3_RS.e6693a18f500754351e42772eccb79a9.mugqic.done
)
estimate_ribosomal_rna_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_4_JOB_ID: bwa_mem_rRNA.Tx1_Days0_Rep4_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days0_Rep4_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days0_Rep4_RS.56814c945fb88e11eec58539684e0f41.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days0_Rep4_RS.56814c945fb88e11eec58539684e0f41.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days0_Rep4/Tx1_Days0_Rep4_RS metrics/Tx1_Days0_Rep4/Tx1_Days0_Rep4_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days0_Rep4/Tx1_Days0_Rep4_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days0_Rep4_RS	SM:Tx1_Days0_Rep4	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days0_Rep4/Tx1_Days0_Rep4_RS/Tx1_Days0_Rep4_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days0_Rep4/Tx1_Days0_Rep4_RS/Tx1_Days0_Rep4_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days0_Rep4/Tx1_Days0_Rep4_RS/Tx1_Days0_Rep4_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days0_Rep4_RS.56814c945fb88e11eec58539684e0f41.mugqic.done
)
estimate_ribosomal_rna_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_5_JOB_ID: bwa_mem_rRNA.Tx1_Days0_Rep5_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days0_Rep5_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days0_Rep5_RS.62211d2d238400c478bb3dca04ab099a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days0_Rep5_RS.62211d2d238400c478bb3dca04ab099a.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days0_Rep5/Tx1_Days0_Rep5_RS metrics/Tx1_Days0_Rep5/Tx1_Days0_Rep5_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days0_Rep5/Tx1_Days0_Rep5_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days0_Rep5_RS	SM:Tx1_Days0_Rep5	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days0_Rep5/Tx1_Days0_Rep5_RS/Tx1_Days0_Rep5_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days0_Rep5/Tx1_Days0_Rep5_RS/Tx1_Days0_Rep5_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days0_Rep5/Tx1_Days0_Rep5_RS/Tx1_Days0_Rep5_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days0_Rep5_RS.62211d2d238400c478bb3dca04ab099a.mugqic.done
)
estimate_ribosomal_rna_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_6_JOB_ID: bwa_mem_rRNA.Tx1_Days0_Rep6_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days0_Rep6_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days0_Rep6_RS.61d13942a25fcb8fc7cadf491c280ee2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days0_Rep6_RS.61d13942a25fcb8fc7cadf491c280ee2.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days0_Rep6/Tx1_Days0_Rep6_RS metrics/Tx1_Days0_Rep6/Tx1_Days0_Rep6_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days0_Rep6/Tx1_Days0_Rep6_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days0_Rep6_RS	SM:Tx1_Days0_Rep6	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days0_Rep6/Tx1_Days0_Rep6_RS/Tx1_Days0_Rep6_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days0_Rep6/Tx1_Days0_Rep6_RS/Tx1_Days0_Rep6_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days0_Rep6/Tx1_Days0_Rep6_RS/Tx1_Days0_Rep6_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days0_Rep6_RS.61d13942a25fcb8fc7cadf491c280ee2.mugqic.done
)
estimate_ribosomal_rna_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_7_JOB_ID: bwa_mem_rRNA.Tx1_Days0_Rep7_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days0_Rep7_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days0_Rep7_RS.eda2b99cf3860d04663c683fbc2b4b3e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days0_Rep7_RS.eda2b99cf3860d04663c683fbc2b4b3e.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days0_Rep7/Tx1_Days0_Rep7_RS metrics/Tx1_Days0_Rep7/Tx1_Days0_Rep7_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days0_Rep7/Tx1_Days0_Rep7_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days0_Rep7_RS	SM:Tx1_Days0_Rep7	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days0_Rep7/Tx1_Days0_Rep7_RS/Tx1_Days0_Rep7_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days0_Rep7/Tx1_Days0_Rep7_RS/Tx1_Days0_Rep7_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days0_Rep7/Tx1_Days0_Rep7_RS/Tx1_Days0_Rep7_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days0_Rep7_RS.eda2b99cf3860d04663c683fbc2b4b3e.mugqic.done
)
estimate_ribosomal_rna_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_8_JOB_ID: bwa_mem_rRNA.Tx1_Days0_Rep8_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days0_Rep8_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days0_Rep8_RS.67ac059457a90c3da392ba8556830da0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days0_Rep8_RS.67ac059457a90c3da392ba8556830da0.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days0_Rep8/Tx1_Days0_Rep8_RS metrics/Tx1_Days0_Rep8/Tx1_Days0_Rep8_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days0_Rep8/Tx1_Days0_Rep8_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days0_Rep8_RS	SM:Tx1_Days0_Rep8	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days0_Rep8/Tx1_Days0_Rep8_RS/Tx1_Days0_Rep8_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days0_Rep8/Tx1_Days0_Rep8_RS/Tx1_Days0_Rep8_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days0_Rep8/Tx1_Days0_Rep8_RS/Tx1_Days0_Rep8_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days0_Rep8_RS.67ac059457a90c3da392ba8556830da0.mugqic.done
)
estimate_ribosomal_rna_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_9_JOB_ID: bwa_mem_rRNA.Tx1_Days2_Rep1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days2_Rep1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days2_Rep1_RS.88c121b046953ce8fc120fe5e1f44e45.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days2_Rep1_RS.88c121b046953ce8fc120fe5e1f44e45.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days2_Rep1/Tx1_Days2_Rep1_RS metrics/Tx1_Days2_Rep1/Tx1_Days2_Rep1_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days2_Rep1/Tx1_Days2_Rep1_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days2_Rep1_RS	SM:Tx1_Days2_Rep1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days2_Rep1/Tx1_Days2_Rep1_RS/Tx1_Days2_Rep1_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days2_Rep1/Tx1_Days2_Rep1_RS/Tx1_Days2_Rep1_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days2_Rep1/Tx1_Days2_Rep1_RS/Tx1_Days2_Rep1_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days2_Rep1_RS.88c121b046953ce8fc120fe5e1f44e45.mugqic.done
)
estimate_ribosomal_rna_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_10_JOB_ID: bwa_mem_rRNA.Tx1_Days2_Rep2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days2_Rep2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days2_Rep2_RS.2caa0ba45c58bfc1661cc543ab3cf853.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days2_Rep2_RS.2caa0ba45c58bfc1661cc543ab3cf853.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days2_Rep2/Tx1_Days2_Rep2_RS metrics/Tx1_Days2_Rep2/Tx1_Days2_Rep2_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days2_Rep2/Tx1_Days2_Rep2_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days2_Rep2_RS	SM:Tx1_Days2_Rep2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days2_Rep2/Tx1_Days2_Rep2_RS/Tx1_Days2_Rep2_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days2_Rep2/Tx1_Days2_Rep2_RS/Tx1_Days2_Rep2_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days2_Rep2/Tx1_Days2_Rep2_RS/Tx1_Days2_Rep2_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days2_Rep2_RS.2caa0ba45c58bfc1661cc543ab3cf853.mugqic.done
)
estimate_ribosomal_rna_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_11_JOB_ID: bwa_mem_rRNA.Tx1_Days2_Rep3_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days2_Rep3_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days2_Rep3_RS.b6c3ce96ded5a1301d8204dbb9a66b0f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days2_Rep3_RS.b6c3ce96ded5a1301d8204dbb9a66b0f.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days2_Rep3/Tx1_Days2_Rep3_RS metrics/Tx1_Days2_Rep3/Tx1_Days2_Rep3_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days2_Rep3/Tx1_Days2_Rep3_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days2_Rep3_RS	SM:Tx1_Days2_Rep3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days2_Rep3/Tx1_Days2_Rep3_RS/Tx1_Days2_Rep3_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days2_Rep3/Tx1_Days2_Rep3_RS/Tx1_Days2_Rep3_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days2_Rep3/Tx1_Days2_Rep3_RS/Tx1_Days2_Rep3_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days2_Rep3_RS.b6c3ce96ded5a1301d8204dbb9a66b0f.mugqic.done
)
estimate_ribosomal_rna_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_12_JOB_ID: bwa_mem_rRNA.Tx1_Days2_Rep4_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days2_Rep4_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days2_Rep4_RS.716fee9e0cf44c3ae1d85e847ba5b372.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days2_Rep4_RS.716fee9e0cf44c3ae1d85e847ba5b372.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days2_Rep4/Tx1_Days2_Rep4_RS metrics/Tx1_Days2_Rep4/Tx1_Days2_Rep4_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days2_Rep4/Tx1_Days2_Rep4_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days2_Rep4_RS	SM:Tx1_Days2_Rep4	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days2_Rep4/Tx1_Days2_Rep4_RS/Tx1_Days2_Rep4_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days2_Rep4/Tx1_Days2_Rep4_RS/Tx1_Days2_Rep4_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days2_Rep4/Tx1_Days2_Rep4_RS/Tx1_Days2_Rep4_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days2_Rep4_RS.716fee9e0cf44c3ae1d85e847ba5b372.mugqic.done
)
estimate_ribosomal_rna_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_13_JOB_ID: bwa_mem_rRNA.Tx1_Days2_Rep5_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days2_Rep5_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days2_Rep5_RS.ad20e308b8951a26d2513fedb1b09402.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days2_Rep5_RS.ad20e308b8951a26d2513fedb1b09402.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days2_Rep5/Tx1_Days2_Rep5_RS metrics/Tx1_Days2_Rep5/Tx1_Days2_Rep5_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days2_Rep5/Tx1_Days2_Rep5_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days2_Rep5_RS	SM:Tx1_Days2_Rep5	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days2_Rep5/Tx1_Days2_Rep5_RS/Tx1_Days2_Rep5_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days2_Rep5/Tx1_Days2_Rep5_RS/Tx1_Days2_Rep5_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days2_Rep5/Tx1_Days2_Rep5_RS/Tx1_Days2_Rep5_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days2_Rep5_RS.ad20e308b8951a26d2513fedb1b09402.mugqic.done
)
estimate_ribosomal_rna_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_14_JOB_ID: bwa_mem_rRNA.Tx1_Days2_Rep6_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days2_Rep6_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days2_Rep6_RS.4a963adf0db9a5f207a4c4dabec5fe81.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days2_Rep6_RS.4a963adf0db9a5f207a4c4dabec5fe81.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days2_Rep6/Tx1_Days2_Rep6_RS metrics/Tx1_Days2_Rep6/Tx1_Days2_Rep6_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days2_Rep6/Tx1_Days2_Rep6_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days2_Rep6_RS	SM:Tx1_Days2_Rep6	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days2_Rep6/Tx1_Days2_Rep6_RS/Tx1_Days2_Rep6_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days2_Rep6/Tx1_Days2_Rep6_RS/Tx1_Days2_Rep6_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days2_Rep6/Tx1_Days2_Rep6_RS/Tx1_Days2_Rep6_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days2_Rep6_RS.4a963adf0db9a5f207a4c4dabec5fe81.mugqic.done
)
estimate_ribosomal_rna_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_15_JOB_ID: bwa_mem_rRNA.Tx1_Days2_Rep7_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days2_Rep7_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days2_Rep7_RS.cbde9df2ddabfdade020a2d5723a4801.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days2_Rep7_RS.cbde9df2ddabfdade020a2d5723a4801.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days2_Rep7/Tx1_Days2_Rep7_RS metrics/Tx1_Days2_Rep7/Tx1_Days2_Rep7_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days2_Rep7/Tx1_Days2_Rep7_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days2_Rep7_RS	SM:Tx1_Days2_Rep7	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days2_Rep7/Tx1_Days2_Rep7_RS/Tx1_Days2_Rep7_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days2_Rep7/Tx1_Days2_Rep7_RS/Tx1_Days2_Rep7_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days2_Rep7/Tx1_Days2_Rep7_RS/Tx1_Days2_Rep7_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days2_Rep7_RS.cbde9df2ddabfdade020a2d5723a4801.mugqic.done
)
estimate_ribosomal_rna_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_16_JOB_ID: bwa_mem_rRNA.Tx1_Days2_Rep8_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days2_Rep8_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days2_Rep8_RS.7753a6acd500498b0c4b9fc748e8167b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days2_Rep8_RS.7753a6acd500498b0c4b9fc748e8167b.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days2_Rep8/Tx1_Days2_Rep8_RS metrics/Tx1_Days2_Rep8/Tx1_Days2_Rep8_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days2_Rep8/Tx1_Days2_Rep8_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days2_Rep8_RS	SM:Tx1_Days2_Rep8	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days2_Rep8/Tx1_Days2_Rep8_RS/Tx1_Days2_Rep8_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days2_Rep8/Tx1_Days2_Rep8_RS/Tx1_Days2_Rep8_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days2_Rep8/Tx1_Days2_Rep8_RS/Tx1_Days2_Rep8_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days2_Rep8_RS.7753a6acd500498b0c4b9fc748e8167b.mugqic.done
)
estimate_ribosomal_rna_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_17_JOB_ID: bwa_mem_rRNA.Tx1_Days8_Rep1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days8_Rep1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days8_Rep1_RS.573aa84f68d92c3eb02bb8988561d300.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days8_Rep1_RS.573aa84f68d92c3eb02bb8988561d300.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days8_Rep1/Tx1_Days8_Rep1_RS metrics/Tx1_Days8_Rep1/Tx1_Days8_Rep1_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days8_Rep1/Tx1_Days8_Rep1_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days8_Rep1_RS	SM:Tx1_Days8_Rep1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days8_Rep1/Tx1_Days8_Rep1_RS/Tx1_Days8_Rep1_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days8_Rep1/Tx1_Days8_Rep1_RS/Tx1_Days8_Rep1_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days8_Rep1/Tx1_Days8_Rep1_RS/Tx1_Days8_Rep1_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days8_Rep1_RS.573aa84f68d92c3eb02bb8988561d300.mugqic.done
)
estimate_ribosomal_rna_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_18_JOB_ID: bwa_mem_rRNA.Tx1_Days8_Rep2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days8_Rep2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days8_Rep2_RS.3b6015353f6aac7a968ec7f89dfa714f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days8_Rep2_RS.3b6015353f6aac7a968ec7f89dfa714f.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days8_Rep2/Tx1_Days8_Rep2_RS metrics/Tx1_Days8_Rep2/Tx1_Days8_Rep2_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days8_Rep2/Tx1_Days8_Rep2_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days8_Rep2_RS	SM:Tx1_Days8_Rep2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days8_Rep2/Tx1_Days8_Rep2_RS/Tx1_Days8_Rep2_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days8_Rep2/Tx1_Days8_Rep2_RS/Tx1_Days8_Rep2_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days8_Rep2/Tx1_Days8_Rep2_RS/Tx1_Days8_Rep2_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days8_Rep2_RS.3b6015353f6aac7a968ec7f89dfa714f.mugqic.done
)
estimate_ribosomal_rna_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_19_JOB_ID: bwa_mem_rRNA.Tx1_Days8_Rep3_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days8_Rep3_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days8_Rep3_RS.ec819ac41614e2e6fe30ddc0c858f01b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days8_Rep3_RS.ec819ac41614e2e6fe30ddc0c858f01b.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days8_Rep3/Tx1_Days8_Rep3_RS metrics/Tx1_Days8_Rep3/Tx1_Days8_Rep3_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days8_Rep3/Tx1_Days8_Rep3_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days8_Rep3_RS	SM:Tx1_Days8_Rep3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days8_Rep3/Tx1_Days8_Rep3_RS/Tx1_Days8_Rep3_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days8_Rep3/Tx1_Days8_Rep3_RS/Tx1_Days8_Rep3_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days8_Rep3/Tx1_Days8_Rep3_RS/Tx1_Days8_Rep3_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days8_Rep3_RS.ec819ac41614e2e6fe30ddc0c858f01b.mugqic.done
)
estimate_ribosomal_rna_19_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_20_JOB_ID: bwa_mem_rRNA.Tx1_Days8_Rep4_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days8_Rep4_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days8_Rep4_RS.0d62170f584ebb633af35e73afa97ca6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days8_Rep4_RS.0d62170f584ebb633af35e73afa97ca6.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days8_Rep4/Tx1_Days8_Rep4_RS metrics/Tx1_Days8_Rep4/Tx1_Days8_Rep4_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days8_Rep4/Tx1_Days8_Rep4_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days8_Rep4_RS	SM:Tx1_Days8_Rep4	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days8_Rep4/Tx1_Days8_Rep4_RS/Tx1_Days8_Rep4_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days8_Rep4/Tx1_Days8_Rep4_RS/Tx1_Days8_Rep4_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days8_Rep4/Tx1_Days8_Rep4_RS/Tx1_Days8_Rep4_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days8_Rep4_RS.0d62170f584ebb633af35e73afa97ca6.mugqic.done
)
estimate_ribosomal_rna_20_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_21_JOB_ID: bwa_mem_rRNA.Tx1_Days8_Rep5_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days8_Rep5_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days8_Rep5_RS.498ed0d830111acb9cf0127bfc5a35f5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days8_Rep5_RS.498ed0d830111acb9cf0127bfc5a35f5.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days8_Rep5/Tx1_Days8_Rep5_RS metrics/Tx1_Days8_Rep5/Tx1_Days8_Rep5_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days8_Rep5/Tx1_Days8_Rep5_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days8_Rep5_RS	SM:Tx1_Days8_Rep5	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days8_Rep5/Tx1_Days8_Rep5_RS/Tx1_Days8_Rep5_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days8_Rep5/Tx1_Days8_Rep5_RS/Tx1_Days8_Rep5_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days8_Rep5/Tx1_Days8_Rep5_RS/Tx1_Days8_Rep5_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days8_Rep5_RS.498ed0d830111acb9cf0127bfc5a35f5.mugqic.done
)
estimate_ribosomal_rna_21_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_22_JOB_ID: bwa_mem_rRNA.Tx1_Days8_Rep6_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days8_Rep6_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days8_Rep6_RS.84f7bd8c2ca6c54d13ac9ca8df3d4a32.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days8_Rep6_RS.84f7bd8c2ca6c54d13ac9ca8df3d4a32.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days8_Rep6/Tx1_Days8_Rep6_RS metrics/Tx1_Days8_Rep6/Tx1_Days8_Rep6_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days8_Rep6/Tx1_Days8_Rep6_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days8_Rep6_RS	SM:Tx1_Days8_Rep6	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days8_Rep6/Tx1_Days8_Rep6_RS/Tx1_Days8_Rep6_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days8_Rep6/Tx1_Days8_Rep6_RS/Tx1_Days8_Rep6_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days8_Rep6/Tx1_Days8_Rep6_RS/Tx1_Days8_Rep6_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days8_Rep6_RS.84f7bd8c2ca6c54d13ac9ca8df3d4a32.mugqic.done
)
estimate_ribosomal_rna_22_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_23_JOB_ID: bwa_mem_rRNA.Tx1_Days8_Rep7_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days8_Rep7_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days8_Rep7_RS.ec5d378f20c02e70edbd58cdeda8fcb1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days8_Rep7_RS.ec5d378f20c02e70edbd58cdeda8fcb1.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days8_Rep7/Tx1_Days8_Rep7_RS metrics/Tx1_Days8_Rep7/Tx1_Days8_Rep7_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days8_Rep7/Tx1_Days8_Rep7_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days8_Rep7_RS	SM:Tx1_Days8_Rep7	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days8_Rep7/Tx1_Days8_Rep7_RS/Tx1_Days8_Rep7_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days8_Rep7/Tx1_Days8_Rep7_RS/Tx1_Days8_Rep7_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days8_Rep7/Tx1_Days8_Rep7_RS/Tx1_Days8_Rep7_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days8_Rep7_RS.ec5d378f20c02e70edbd58cdeda8fcb1.mugqic.done
)
estimate_ribosomal_rna_23_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_24_JOB_ID: bwa_mem_rRNA.Tx1_Days8_Rep8_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx1_Days8_Rep8_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx1_Days8_Rep8_RS.052ef9cb6d7c2d5291c2ac801f820415.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx1_Days8_Rep8_RS.052ef9cb6d7c2d5291c2ac801f820415.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx1_Days8_Rep8/Tx1_Days8_Rep8_RS metrics/Tx1_Days8_Rep8/Tx1_Days8_Rep8_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx1_Days8_Rep8/Tx1_Days8_Rep8_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx1_Days8_Rep8_RS	SM:Tx1_Days8_Rep8	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx1_Days8_Rep8/Tx1_Days8_Rep8_RS/Tx1_Days8_Rep8_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx1_Days8_Rep8/Tx1_Days8_Rep8_RS/Tx1_Days8_Rep8_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx1_Days8_Rep8/Tx1_Days8_Rep8_RS/Tx1_Days8_Rep8_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx1_Days8_Rep8_RS.052ef9cb6d7c2d5291c2ac801f820415.mugqic.done
)
estimate_ribosomal_rna_24_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_25_JOB_ID: bwa_mem_rRNA.Tx3_Days0_Rep1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days0_Rep1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days0_Rep1_RS.f32b23078338249e6b6d7b960f4fa17d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days0_Rep1_RS.f32b23078338249e6b6d7b960f4fa17d.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days0_Rep1/Tx3_Days0_Rep1_RS metrics/Tx3_Days0_Rep1/Tx3_Days0_Rep1_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days0_Rep1/Tx3_Days0_Rep1_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days0_Rep1_RS	SM:Tx3_Days0_Rep1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days0_Rep1/Tx3_Days0_Rep1_RS/Tx3_Days0_Rep1_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days0_Rep1/Tx3_Days0_Rep1_RS/Tx3_Days0_Rep1_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days0_Rep1/Tx3_Days0_Rep1_RS/Tx3_Days0_Rep1_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days0_Rep1_RS.f32b23078338249e6b6d7b960f4fa17d.mugqic.done
)
estimate_ribosomal_rna_25_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_25_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_26_JOB_ID: bwa_mem_rRNA.Tx3_Days0_Rep2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days0_Rep2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days0_Rep2_RS.50cebdc06369fa6cac5e227f3c25e5b8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days0_Rep2_RS.50cebdc06369fa6cac5e227f3c25e5b8.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days0_Rep2/Tx3_Days0_Rep2_RS metrics/Tx3_Days0_Rep2/Tx3_Days0_Rep2_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days0_Rep2/Tx3_Days0_Rep2_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days0_Rep2_RS	SM:Tx3_Days0_Rep2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days0_Rep2/Tx3_Days0_Rep2_RS/Tx3_Days0_Rep2_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days0_Rep2/Tx3_Days0_Rep2_RS/Tx3_Days0_Rep2_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days0_Rep2/Tx3_Days0_Rep2_RS/Tx3_Days0_Rep2_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days0_Rep2_RS.50cebdc06369fa6cac5e227f3c25e5b8.mugqic.done
)
estimate_ribosomal_rna_26_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_26_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_27_JOB_ID: bwa_mem_rRNA.Tx3_Days0_Rep3_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days0_Rep3_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days0_Rep3_RS.7bbed8fc6eaa667fc9ab5ba41e98330f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days0_Rep3_RS.7bbed8fc6eaa667fc9ab5ba41e98330f.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days0_Rep3/Tx3_Days0_Rep3_RS metrics/Tx3_Days0_Rep3/Tx3_Days0_Rep3_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days0_Rep3/Tx3_Days0_Rep3_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days0_Rep3_RS	SM:Tx3_Days0_Rep3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days0_Rep3/Tx3_Days0_Rep3_RS/Tx3_Days0_Rep3_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days0_Rep3/Tx3_Days0_Rep3_RS/Tx3_Days0_Rep3_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days0_Rep3/Tx3_Days0_Rep3_RS/Tx3_Days0_Rep3_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days0_Rep3_RS.7bbed8fc6eaa667fc9ab5ba41e98330f.mugqic.done
)
estimate_ribosomal_rna_27_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_27_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_28_JOB_ID: bwa_mem_rRNA.Tx3_Days0_Rep4_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days0_Rep4_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days0_Rep4_RS.e208c7d2e92dac681fcbf634ff5f1f1d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days0_Rep4_RS.e208c7d2e92dac681fcbf634ff5f1f1d.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days0_Rep4/Tx3_Days0_Rep4_RS metrics/Tx3_Days0_Rep4/Tx3_Days0_Rep4_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days0_Rep4/Tx3_Days0_Rep4_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days0_Rep4_RS	SM:Tx3_Days0_Rep4	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days0_Rep4/Tx3_Days0_Rep4_RS/Tx3_Days0_Rep4_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days0_Rep4/Tx3_Days0_Rep4_RS/Tx3_Days0_Rep4_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days0_Rep4/Tx3_Days0_Rep4_RS/Tx3_Days0_Rep4_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days0_Rep4_RS.e208c7d2e92dac681fcbf634ff5f1f1d.mugqic.done
)
estimate_ribosomal_rna_28_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_28_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_29_JOB_ID: bwa_mem_rRNA.Tx3_Days0_Rep5_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days0_Rep5_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days0_Rep5_RS.c061a53f1079266f51c5620f5b981bf5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days0_Rep5_RS.c061a53f1079266f51c5620f5b981bf5.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days0_Rep5/Tx3_Days0_Rep5_RS metrics/Tx3_Days0_Rep5/Tx3_Days0_Rep5_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days0_Rep5/Tx3_Days0_Rep5_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days0_Rep5_RS	SM:Tx3_Days0_Rep5	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days0_Rep5/Tx3_Days0_Rep5_RS/Tx3_Days0_Rep5_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days0_Rep5/Tx3_Days0_Rep5_RS/Tx3_Days0_Rep5_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days0_Rep5/Tx3_Days0_Rep5_RS/Tx3_Days0_Rep5_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days0_Rep5_RS.c061a53f1079266f51c5620f5b981bf5.mugqic.done
)
estimate_ribosomal_rna_29_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_29_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_30_JOB_ID: bwa_mem_rRNA.Tx3_Days0_Rep6_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days0_Rep6_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days0_Rep6_RS.1a558a4f97ab735ffc8982ed2f66f438.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days0_Rep6_RS.1a558a4f97ab735ffc8982ed2f66f438.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days0_Rep6/Tx3_Days0_Rep6_RS metrics/Tx3_Days0_Rep6/Tx3_Days0_Rep6_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days0_Rep6/Tx3_Days0_Rep6_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days0_Rep6_RS	SM:Tx3_Days0_Rep6	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days0_Rep6/Tx3_Days0_Rep6_RS/Tx3_Days0_Rep6_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days0_Rep6/Tx3_Days0_Rep6_RS/Tx3_Days0_Rep6_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days0_Rep6/Tx3_Days0_Rep6_RS/Tx3_Days0_Rep6_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days0_Rep6_RS.1a558a4f97ab735ffc8982ed2f66f438.mugqic.done
)
estimate_ribosomal_rna_30_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_30_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_31_JOB_ID: bwa_mem_rRNA.Tx3_Days0_Rep7_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days0_Rep7_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days0_Rep7_RS.809d5d63779a32b42382cf5b64d72501.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days0_Rep7_RS.809d5d63779a32b42382cf5b64d72501.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days0_Rep7/Tx3_Days0_Rep7_RS metrics/Tx3_Days0_Rep7/Tx3_Days0_Rep7_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days0_Rep7/Tx3_Days0_Rep7_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days0_Rep7_RS	SM:Tx3_Days0_Rep7	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days0_Rep7/Tx3_Days0_Rep7_RS/Tx3_Days0_Rep7_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days0_Rep7/Tx3_Days0_Rep7_RS/Tx3_Days0_Rep7_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days0_Rep7/Tx3_Days0_Rep7_RS/Tx3_Days0_Rep7_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days0_Rep7_RS.809d5d63779a32b42382cf5b64d72501.mugqic.done
)
estimate_ribosomal_rna_31_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_31_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_32_JOB_ID: bwa_mem_rRNA.Tx3_Days0_Rep8_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days0_Rep8_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days0_Rep8_RS.63dc145950ac1c811d59639daec828dc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days0_Rep8_RS.63dc145950ac1c811d59639daec828dc.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days0_Rep8/Tx3_Days0_Rep8_RS metrics/Tx3_Days0_Rep8/Tx3_Days0_Rep8_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days0_Rep8/Tx3_Days0_Rep8_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days0_Rep8_RS	SM:Tx3_Days0_Rep8	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days0_Rep8/Tx3_Days0_Rep8_RS/Tx3_Days0_Rep8_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days0_Rep8/Tx3_Days0_Rep8_RS/Tx3_Days0_Rep8_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days0_Rep8/Tx3_Days0_Rep8_RS/Tx3_Days0_Rep8_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days0_Rep8_RS.63dc145950ac1c811d59639daec828dc.mugqic.done
)
estimate_ribosomal_rna_32_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_32_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_33_JOB_ID: bwa_mem_rRNA.Tx3_Days2_Rep1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days2_Rep1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days2_Rep1_RS.40a805f5993385f65a0384bc83d14b62.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days2_Rep1_RS.40a805f5993385f65a0384bc83d14b62.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days2_Rep1/Tx3_Days2_Rep1_RS metrics/Tx3_Days2_Rep1/Tx3_Days2_Rep1_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days2_Rep1/Tx3_Days2_Rep1_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days2_Rep1_RS	SM:Tx3_Days2_Rep1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days2_Rep1/Tx3_Days2_Rep1_RS/Tx3_Days2_Rep1_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days2_Rep1/Tx3_Days2_Rep1_RS/Tx3_Days2_Rep1_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days2_Rep1/Tx3_Days2_Rep1_RS/Tx3_Days2_Rep1_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days2_Rep1_RS.40a805f5993385f65a0384bc83d14b62.mugqic.done
)
estimate_ribosomal_rna_33_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_33_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_34_JOB_ID: bwa_mem_rRNA.Tx3_Days2_Rep2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days2_Rep2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days2_Rep2_RS.97bd4ce41ff4890625ab6aa48c3c7ed2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days2_Rep2_RS.97bd4ce41ff4890625ab6aa48c3c7ed2.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days2_Rep2/Tx3_Days2_Rep2_RS metrics/Tx3_Days2_Rep2/Tx3_Days2_Rep2_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days2_Rep2/Tx3_Days2_Rep2_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days2_Rep2_RS	SM:Tx3_Days2_Rep2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days2_Rep2/Tx3_Days2_Rep2_RS/Tx3_Days2_Rep2_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days2_Rep2/Tx3_Days2_Rep2_RS/Tx3_Days2_Rep2_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days2_Rep2/Tx3_Days2_Rep2_RS/Tx3_Days2_Rep2_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days2_Rep2_RS.97bd4ce41ff4890625ab6aa48c3c7ed2.mugqic.done
)
estimate_ribosomal_rna_34_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_34_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_35_JOB_ID: bwa_mem_rRNA.Tx3_Days2_Rep3_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days2_Rep3_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days2_Rep3_RS.8d981921cdae4eb918c26c76eaa17c25.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days2_Rep3_RS.8d981921cdae4eb918c26c76eaa17c25.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days2_Rep3/Tx3_Days2_Rep3_RS metrics/Tx3_Days2_Rep3/Tx3_Days2_Rep3_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days2_Rep3/Tx3_Days2_Rep3_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days2_Rep3_RS	SM:Tx3_Days2_Rep3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days2_Rep3/Tx3_Days2_Rep3_RS/Tx3_Days2_Rep3_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days2_Rep3/Tx3_Days2_Rep3_RS/Tx3_Days2_Rep3_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days2_Rep3/Tx3_Days2_Rep3_RS/Tx3_Days2_Rep3_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days2_Rep3_RS.8d981921cdae4eb918c26c76eaa17c25.mugqic.done
)
estimate_ribosomal_rna_35_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_35_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_36_JOB_ID: bwa_mem_rRNA.Tx3_Days2_Rep4_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days2_Rep4_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days2_Rep4_RS.2840da7a06194abd50d251a729a2f389.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days2_Rep4_RS.2840da7a06194abd50d251a729a2f389.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days2_Rep4/Tx3_Days2_Rep4_RS metrics/Tx3_Days2_Rep4/Tx3_Days2_Rep4_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days2_Rep4/Tx3_Days2_Rep4_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days2_Rep4_RS	SM:Tx3_Days2_Rep4	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days2_Rep4/Tx3_Days2_Rep4_RS/Tx3_Days2_Rep4_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days2_Rep4/Tx3_Days2_Rep4_RS/Tx3_Days2_Rep4_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days2_Rep4/Tx3_Days2_Rep4_RS/Tx3_Days2_Rep4_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days2_Rep4_RS.2840da7a06194abd50d251a729a2f389.mugqic.done
)
estimate_ribosomal_rna_36_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_36_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_37_JOB_ID: bwa_mem_rRNA.Tx3_Days2_Rep5_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days2_Rep5_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days2_Rep5_RS.a9eacbb9a7d5fe28432ab020c52daa1d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days2_Rep5_RS.a9eacbb9a7d5fe28432ab020c52daa1d.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days2_Rep5/Tx3_Days2_Rep5_RS metrics/Tx3_Days2_Rep5/Tx3_Days2_Rep5_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days2_Rep5/Tx3_Days2_Rep5_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days2_Rep5_RS	SM:Tx3_Days2_Rep5	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days2_Rep5/Tx3_Days2_Rep5_RS/Tx3_Days2_Rep5_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days2_Rep5/Tx3_Days2_Rep5_RS/Tx3_Days2_Rep5_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days2_Rep5/Tx3_Days2_Rep5_RS/Tx3_Days2_Rep5_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days2_Rep5_RS.a9eacbb9a7d5fe28432ab020c52daa1d.mugqic.done
)
estimate_ribosomal_rna_37_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_37_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_38_JOB_ID: bwa_mem_rRNA.Tx3_Days2_Rep6_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days2_Rep6_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days2_Rep6_RS.0ed66a6707ef3cd808f977b3386a87cd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days2_Rep6_RS.0ed66a6707ef3cd808f977b3386a87cd.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days2_Rep6/Tx3_Days2_Rep6_RS metrics/Tx3_Days2_Rep6/Tx3_Days2_Rep6_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days2_Rep6/Tx3_Days2_Rep6_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days2_Rep6_RS	SM:Tx3_Days2_Rep6	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days2_Rep6/Tx3_Days2_Rep6_RS/Tx3_Days2_Rep6_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days2_Rep6/Tx3_Days2_Rep6_RS/Tx3_Days2_Rep6_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days2_Rep6/Tx3_Days2_Rep6_RS/Tx3_Days2_Rep6_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days2_Rep6_RS.0ed66a6707ef3cd808f977b3386a87cd.mugqic.done
)
estimate_ribosomal_rna_38_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_38_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_39_JOB_ID: bwa_mem_rRNA.Tx3_Days2_Rep7_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days2_Rep7_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days2_Rep7_RS.2968493752c8ffeea415b4f03c832448.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days2_Rep7_RS.2968493752c8ffeea415b4f03c832448.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days2_Rep7/Tx3_Days2_Rep7_RS metrics/Tx3_Days2_Rep7/Tx3_Days2_Rep7_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days2_Rep7/Tx3_Days2_Rep7_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days2_Rep7_RS	SM:Tx3_Days2_Rep7	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days2_Rep7/Tx3_Days2_Rep7_RS/Tx3_Days2_Rep7_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days2_Rep7/Tx3_Days2_Rep7_RS/Tx3_Days2_Rep7_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days2_Rep7/Tx3_Days2_Rep7_RS/Tx3_Days2_Rep7_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days2_Rep7_RS.2968493752c8ffeea415b4f03c832448.mugqic.done
)
estimate_ribosomal_rna_39_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_39_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_40_JOB_ID: bwa_mem_rRNA.Tx3_Days2_Rep8_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days2_Rep8_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days2_Rep8_RS.78137245951cbc07db92bf186e0bf2dc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days2_Rep8_RS.78137245951cbc07db92bf186e0bf2dc.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days2_Rep8/Tx3_Days2_Rep8_RS metrics/Tx3_Days2_Rep8/Tx3_Days2_Rep8_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days2_Rep8/Tx3_Days2_Rep8_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days2_Rep8_RS	SM:Tx3_Days2_Rep8	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days2_Rep8/Tx3_Days2_Rep8_RS/Tx3_Days2_Rep8_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days2_Rep8/Tx3_Days2_Rep8_RS/Tx3_Days2_Rep8_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days2_Rep8/Tx3_Days2_Rep8_RS/Tx3_Days2_Rep8_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days2_Rep8_RS.78137245951cbc07db92bf186e0bf2dc.mugqic.done
)
estimate_ribosomal_rna_40_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_40_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_41_JOB_ID: bwa_mem_rRNA.Tx3_Days8_Rep1_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days8_Rep1_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days8_Rep1_RS.01119e439483915cc97179930f8fa14e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days8_Rep1_RS.01119e439483915cc97179930f8fa14e.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days8_Rep1/Tx3_Days8_Rep1_RS metrics/Tx3_Days8_Rep1/Tx3_Days8_Rep1_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days8_Rep1/Tx3_Days8_Rep1_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days8_Rep1_RS	SM:Tx3_Days8_Rep1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days8_Rep1/Tx3_Days8_Rep1_RS/Tx3_Days8_Rep1_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days8_Rep1/Tx3_Days8_Rep1_RS/Tx3_Days8_Rep1_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days8_Rep1/Tx3_Days8_Rep1_RS/Tx3_Days8_Rep1_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days8_Rep1_RS.01119e439483915cc97179930f8fa14e.mugqic.done
)
estimate_ribosomal_rna_41_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_41_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_42_JOB_ID: bwa_mem_rRNA.Tx3_Days8_Rep2_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days8_Rep2_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days8_Rep2_RS.f8aff7ea0f6350fffcec478216f3e2c9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days8_Rep2_RS.f8aff7ea0f6350fffcec478216f3e2c9.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days8_Rep2/Tx3_Days8_Rep2_RS metrics/Tx3_Days8_Rep2/Tx3_Days8_Rep2_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days8_Rep2/Tx3_Days8_Rep2_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days8_Rep2_RS	SM:Tx3_Days8_Rep2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days8_Rep2/Tx3_Days8_Rep2_RS/Tx3_Days8_Rep2_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days8_Rep2/Tx3_Days8_Rep2_RS/Tx3_Days8_Rep2_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days8_Rep2/Tx3_Days8_Rep2_RS/Tx3_Days8_Rep2_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days8_Rep2_RS.f8aff7ea0f6350fffcec478216f3e2c9.mugqic.done
)
estimate_ribosomal_rna_42_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_42_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_43_JOB_ID: bwa_mem_rRNA.Tx3_Days8_Rep3_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days8_Rep3_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days8_Rep3_RS.e5c626f25de1c63b2bd9f15ab9015aba.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days8_Rep3_RS.e5c626f25de1c63b2bd9f15ab9015aba.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days8_Rep3/Tx3_Days8_Rep3_RS metrics/Tx3_Days8_Rep3/Tx3_Days8_Rep3_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days8_Rep3/Tx3_Days8_Rep3_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days8_Rep3_RS	SM:Tx3_Days8_Rep3	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days8_Rep3/Tx3_Days8_Rep3_RS/Tx3_Days8_Rep3_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days8_Rep3/Tx3_Days8_Rep3_RS/Tx3_Days8_Rep3_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days8_Rep3/Tx3_Days8_Rep3_RS/Tx3_Days8_Rep3_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days8_Rep3_RS.e5c626f25de1c63b2bd9f15ab9015aba.mugqic.done
)
estimate_ribosomal_rna_43_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_43_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_44_JOB_ID: bwa_mem_rRNA.Tx3_Days8_Rep4_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days8_Rep4_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days8_Rep4_RS.1115147e89523bbf6f23cfa4a655b394.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days8_Rep4_RS.1115147e89523bbf6f23cfa4a655b394.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days8_Rep4/Tx3_Days8_Rep4_RS metrics/Tx3_Days8_Rep4/Tx3_Days8_Rep4_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days8_Rep4/Tx3_Days8_Rep4_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days8_Rep4_RS	SM:Tx3_Days8_Rep4	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days8_Rep4/Tx3_Days8_Rep4_RS/Tx3_Days8_Rep4_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days8_Rep4/Tx3_Days8_Rep4_RS/Tx3_Days8_Rep4_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days8_Rep4/Tx3_Days8_Rep4_RS/Tx3_Days8_Rep4_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days8_Rep4_RS.1115147e89523bbf6f23cfa4a655b394.mugqic.done
)
estimate_ribosomal_rna_44_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_44_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_45_JOB_ID: bwa_mem_rRNA.Tx3_Days8_Rep5_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days8_Rep5_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days8_Rep5_RS.2a0c0d71bb88ec38b8b5a2f626ba5cfa.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days8_Rep5_RS.2a0c0d71bb88ec38b8b5a2f626ba5cfa.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days8_Rep5/Tx3_Days8_Rep5_RS metrics/Tx3_Days8_Rep5/Tx3_Days8_Rep5_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days8_Rep5/Tx3_Days8_Rep5_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days8_Rep5_RS	SM:Tx3_Days8_Rep5	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days8_Rep5/Tx3_Days8_Rep5_RS/Tx3_Days8_Rep5_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days8_Rep5/Tx3_Days8_Rep5_RS/Tx3_Days8_Rep5_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days8_Rep5/Tx3_Days8_Rep5_RS/Tx3_Days8_Rep5_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days8_Rep5_RS.2a0c0d71bb88ec38b8b5a2f626ba5cfa.mugqic.done
)
estimate_ribosomal_rna_45_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_45_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_46_JOB_ID: bwa_mem_rRNA.Tx3_Days8_Rep6_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days8_Rep6_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days8_Rep6_RS.158cfbf7648de8dab148eefe12b08a03.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days8_Rep6_RS.158cfbf7648de8dab148eefe12b08a03.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days8_Rep6/Tx3_Days8_Rep6_RS metrics/Tx3_Days8_Rep6/Tx3_Days8_Rep6_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days8_Rep6/Tx3_Days8_Rep6_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days8_Rep6_RS	SM:Tx3_Days8_Rep6	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days8_Rep6/Tx3_Days8_Rep6_RS/Tx3_Days8_Rep6_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days8_Rep6/Tx3_Days8_Rep6_RS/Tx3_Days8_Rep6_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days8_Rep6/Tx3_Days8_Rep6_RS/Tx3_Days8_Rep6_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days8_Rep6_RS.158cfbf7648de8dab148eefe12b08a03.mugqic.done
)
estimate_ribosomal_rna_46_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_46_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_47_JOB_ID: bwa_mem_rRNA.Tx3_Days8_Rep7_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days8_Rep7_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days8_Rep7_RS.3163ca0efaf89664814e4b10f4b457df.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days8_Rep7_RS.3163ca0efaf89664814e4b10f4b457df.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days8_Rep7/Tx3_Days8_Rep7_RS metrics/Tx3_Days8_Rep7/Tx3_Days8_Rep7_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days8_Rep7/Tx3_Days8_Rep7_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days8_Rep7_RS	SM:Tx3_Days8_Rep7	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days8_Rep7/Tx3_Days8_Rep7_RS/Tx3_Days8_Rep7_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days8_Rep7/Tx3_Days8_Rep7_RS/Tx3_Days8_Rep7_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days8_Rep7/Tx3_Days8_Rep7_RS/Tx3_Days8_Rep7_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days8_Rep7_RS.3163ca0efaf89664814e4b10f4b457df.mugqic.done
)
estimate_ribosomal_rna_47_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_47_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_48_JOB_ID: bwa_mem_rRNA.Tx3_Days8_Rep8_RS
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Tx3_Days8_Rep8_RS
JOB_DEPENDENCIES=
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Tx3_Days8_Rep8_RS.f5834729c3b61b39e9f66c84ba34da9d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Tx3_Days8_Rep8_RS.f5834729c3b61b39e9f66c84ba34da9d.mugqic.done'
module load java/1.8.0_121 mugqic/bvatools/1.6 mugqic/bwa/0.7.15 mugqic/picard/2.9.0 mugqic/mugqic_tools/2.1.9 mugqic/python/2.7.13 && \
mkdir -p alignment/Tx3_Days8_Rep8/Tx3_Days8_Rep8_RS metrics/Tx3_Days8_Rep8/Tx3_Days8_Rep8_RS && \
java -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Tx3_Days8_Rep8/Tx3_Days8_Rep8_RS/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Tx3_Days8_Rep8_RS	SM:Tx3_Days8_Rep8	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /scratch/efournie/YM_rnaseq/ref/annotations/rrna_bwa_index/Sus_scrofa.Sscrofa11.1.Ensembl93.rrna.fa \
  /dev/stdin | \
 java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=1  -Dsamjdk.buffer_size=1048576 -Xmx7G -jar $PICARD_HOME/picard.jar SortSam \
 VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
 TMP_DIR=${SLURM_TMPDIR} \
 INPUT=/dev/stdin \
 OUTPUT=metrics/Tx3_Days8_Rep8/Tx3_Days8_Rep8_RS/Tx3_Days8_Rep8_RSrRNA.bam \
 SORT_ORDER=coordinate \
 MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Tx3_Days8_Rep8/Tx3_Days8_Rep8_RS/Tx3_Days8_Rep8_RSrRNA.bam \
  -g /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
  -o metrics/Tx3_Days8_Rep8/Tx3_Days8_Rep8_RS/Tx3_Days8_Rep8_RSrRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Tx3_Days8_Rep8_RS.f5834729c3b61b39e9f66c84ba34da9d.mugqic.done
)
estimate_ribosomal_rna_48_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"estimate_ribosomal_rna\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$estimate_ribosomal_rna_48_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: rnaseqc
#-------------------------------------------------------------------------------
STEP=rnaseqc
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: rnaseqc_1_JOB_ID: rnaseqc
#-------------------------------------------------------------------------------
JOB_NAME=rnaseqc
JOB_DEPENDENCIES=
JOB_DONE=job_output/rnaseqc/rnaseqc.8180800c8833dbb045af946fa504a664.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'rnaseqc.8180800c8833dbb045af946fa504a664.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bwa/0.7.15 mugqic/rnaseqc/1.1.8 && \
mkdir -p metrics/rnaseqRep && \
echo "Sample	BamFile	Note
Tx1_Days0_Rep1	alignment/Tx1_Days0_Rep1/Tx1_Days0_Rep1.sorted.mdup.bam	RNAseq
Tx1_Days0_Rep2	alignment/Tx1_Days0_Rep2/Tx1_Days0_Rep2.sorted.mdup.bam	RNAseq
Tx1_Days0_Rep3	alignment/Tx1_Days0_Rep3/Tx1_Days0_Rep3.sorted.mdup.bam	RNAseq
Tx1_Days0_Rep4	alignment/Tx1_Days0_Rep4/Tx1_Days0_Rep4.sorted.mdup.bam	RNAseq
Tx1_Days0_Rep5	alignment/Tx1_Days0_Rep5/Tx1_Days0_Rep5.sorted.mdup.bam	RNAseq
Tx1_Days0_Rep6	alignment/Tx1_Days0_Rep6/Tx1_Days0_Rep6.sorted.mdup.bam	RNAseq
Tx1_Days0_Rep7	alignment/Tx1_Days0_Rep7/Tx1_Days0_Rep7.sorted.mdup.bam	RNAseq
Tx1_Days0_Rep8	alignment/Tx1_Days0_Rep8/Tx1_Days0_Rep8.sorted.mdup.bam	RNAseq
Tx1_Days2_Rep1	alignment/Tx1_Days2_Rep1/Tx1_Days2_Rep1.sorted.mdup.bam	RNAseq
Tx1_Days2_Rep2	alignment/Tx1_Days2_Rep2/Tx1_Days2_Rep2.sorted.mdup.bam	RNAseq
Tx1_Days2_Rep3	alignment/Tx1_Days2_Rep3/Tx1_Days2_Rep3.sorted.mdup.bam	RNAseq
Tx1_Days2_Rep4	alignment/Tx1_Days2_Rep4/Tx1_Days2_Rep4.sorted.mdup.bam	RNAseq
Tx1_Days2_Rep5	alignment/Tx1_Days2_Rep5/Tx1_Days2_Rep5.sorted.mdup.bam	RNAseq
Tx1_Days2_Rep6	alignment/Tx1_Days2_Rep6/Tx1_Days2_Rep6.sorted.mdup.bam	RNAseq
Tx1_Days2_Rep7	alignment/Tx1_Days2_Rep7/Tx1_Days2_Rep7.sorted.mdup.bam	RNAseq
Tx1_Days2_Rep8	alignment/Tx1_Days2_Rep8/Tx1_Days2_Rep8.sorted.mdup.bam	RNAseq
Tx1_Days8_Rep1	alignment/Tx1_Days8_Rep1/Tx1_Days8_Rep1.sorted.mdup.bam	RNAseq
Tx1_Days8_Rep2	alignment/Tx1_Days8_Rep2/Tx1_Days8_Rep2.sorted.mdup.bam	RNAseq
Tx1_Days8_Rep3	alignment/Tx1_Days8_Rep3/Tx1_Days8_Rep3.sorted.mdup.bam	RNAseq
Tx1_Days8_Rep4	alignment/Tx1_Days8_Rep4/Tx1_Days8_Rep4.sorted.mdup.bam	RNAseq
Tx1_Days8_Rep5	alignment/Tx1_Days8_Rep5/Tx1_Days8_Rep5.sorted.mdup.bam	RNAseq
Tx1_Days8_Rep6	alignment/Tx1_Days8_Rep6/Tx1_Days8_Rep6.sorted.mdup.bam	RNAseq
Tx1_Days8_Rep7	alignment/Tx1_Days8_Rep7/Tx1_Days8_Rep7.sorted.mdup.bam	RNAseq
Tx1_Days8_Rep8	alignment/Tx1_Days8_Rep8/Tx1_Days8_Rep8.sorted.mdup.bam	RNAseq
Tx3_Days0_Rep1	alignment/Tx3_Days0_Rep1/Tx3_Days0_Rep1.sorted.mdup.bam	RNAseq
Tx3_Days0_Rep2	alignment/Tx3_Days0_Rep2/Tx3_Days0_Rep2.sorted.mdup.bam	RNAseq
Tx3_Days0_Rep3	alignment/Tx3_Days0_Rep3/Tx3_Days0_Rep3.sorted.mdup.bam	RNAseq
Tx3_Days0_Rep4	alignment/Tx3_Days0_Rep4/Tx3_Days0_Rep4.sorted.mdup.bam	RNAseq
Tx3_Days0_Rep5	alignment/Tx3_Days0_Rep5/Tx3_Days0_Rep5.sorted.mdup.bam	RNAseq
Tx3_Days0_Rep6	alignment/Tx3_Days0_Rep6/Tx3_Days0_Rep6.sorted.mdup.bam	RNAseq
Tx3_Days0_Rep7	alignment/Tx3_Days0_Rep7/Tx3_Days0_Rep7.sorted.mdup.bam	RNAseq
Tx3_Days0_Rep8	alignment/Tx3_Days0_Rep8/Tx3_Days0_Rep8.sorted.mdup.bam	RNAseq
Tx3_Days2_Rep1	alignment/Tx3_Days2_Rep1/Tx3_Days2_Rep1.sorted.mdup.bam	RNAseq
Tx3_Days2_Rep2	alignment/Tx3_Days2_Rep2/Tx3_Days2_Rep2.sorted.mdup.bam	RNAseq
Tx3_Days2_Rep3	alignment/Tx3_Days2_Rep3/Tx3_Days2_Rep3.sorted.mdup.bam	RNAseq
Tx3_Days2_Rep4	alignment/Tx3_Days2_Rep4/Tx3_Days2_Rep4.sorted.mdup.bam	RNAseq
Tx3_Days2_Rep5	alignment/Tx3_Days2_Rep5/Tx3_Days2_Rep5.sorted.mdup.bam	RNAseq
Tx3_Days2_Rep6	alignment/Tx3_Days2_Rep6/Tx3_Days2_Rep6.sorted.mdup.bam	RNAseq
Tx3_Days2_Rep7	alignment/Tx3_Days2_Rep7/Tx3_Days2_Rep7.sorted.mdup.bam	RNAseq
Tx3_Days2_Rep8	alignment/Tx3_Days2_Rep8/Tx3_Days2_Rep8.sorted.mdup.bam	RNAseq
Tx3_Days8_Rep1	alignment/Tx3_Days8_Rep1/Tx3_Days8_Rep1.sorted.mdup.bam	RNAseq
Tx3_Days8_Rep2	alignment/Tx3_Days8_Rep2/Tx3_Days8_Rep2.sorted.mdup.bam	RNAseq
Tx3_Days8_Rep3	alignment/Tx3_Days8_Rep3/Tx3_Days8_Rep3.sorted.mdup.bam	RNAseq
Tx3_Days8_Rep4	alignment/Tx3_Days8_Rep4/Tx3_Days8_Rep4.sorted.mdup.bam	RNAseq
Tx3_Days8_Rep5	alignment/Tx3_Days8_Rep5/Tx3_Days8_Rep5.sorted.mdup.bam	RNAseq
Tx3_Days8_Rep6	alignment/Tx3_Days8_Rep6/Tx3_Days8_Rep6.sorted.mdup.bam	RNAseq
Tx3_Days8_Rep7	alignment/Tx3_Days8_Rep7/Tx3_Days8_Rep7.sorted.mdup.bam	RNAseq
Tx3_Days8_Rep8	alignment/Tx3_Days8_Rep8/Tx3_Days8_Rep8.sorted.mdup.bam	RNAseq" \
  > alignment/rnaseqc.samples.txt && \
touch dummy_rRNA.fa && \
java -Djava.io.tmpdir=${SLURM_TMPDIR} -XX:ParallelGCThreads=4 -Dsamjdk.buffer_size=1048576 -Xmx40G -jar $RNASEQC_JAR \
  -n 1000 \
  -o metrics/rnaseqRep \
  -r /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
  -s alignment/rnaseqc.samples.txt \
  -t /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.93.transcript_id.gtf \
  -ttype 2\
  -BWArRNA dummy_rRNA.fa && \
zip -r metrics/rnaseqRep.zip metrics/rnaseqRep
rnaseqc.8180800c8833dbb045af946fa504a664.mugqic.done
)
rnaseqc_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"rnaseqc\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"rnaseqc\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=72:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$rnaseqc_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: rnaseqc_2_JOB_ID: rnaseqc_report
#-------------------------------------------------------------------------------
JOB_NAME=rnaseqc_report
JOB_DEPENDENCIES=$rnaseqc_1_JOB_ID
JOB_DONE=job_output/rnaseqc/rnaseqc_report.67821e702049231d38161c5db01ef648.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'rnaseqc_report.67821e702049231d38161c5db01ef648.mugqic.done'
module load mugqic/python/2.7.13 mugqic/pandoc/1.15.2 && \
mkdir -p report && \
cp metrics/rnaseqRep.zip report/reportRNAseqQC.zip && \
python -c 'import csv; csv_in = csv.DictReader(open("metrics/rnaseqRep/metrics.tsv"), delimiter="	")
print "	".join(["Sample", "Aligned Reads", "Alternative Alignments", "%", "rRNA Reads", "Coverage", "Exonic Rate", "Genes"])
print "\n".join(["	".join([
    line["Sample"],
    line["Mapped"],
    line["Alternative Aligments"],
    str(float(line["Alternative Aligments"]) / float(line["Mapped"]) * 100),
    line["rRNA"],
    line["Mean Per Base Cov."],
    line["Exonic Rate"],
    line["Genes Detected"]
]) for line in csv_in])' \
  > report/trimAlignmentTable.tsv.tmp && \
if [[ -f metrics/trimSampleTable.tsv ]]
then
  awk -F"	" 'FNR==NR{raw_reads[$1]=$2; surviving_reads[$1]=$3; surviving_pct[$1]=$4; next}{OFS="	"; if ($2=="Aligned Reads"){surviving_pct[$1]="%"; aligned_pct="%"; rrna_pct="%"} else {aligned_pct=($2 / surviving_reads[$1] * 100); rrna_pct=($5 / surviving_reads[$1] * 100)}; printf $1"	"raw_reads[$1]"	"surviving_reads[$1]"	"surviving_pct[$1]"	"$2"	"aligned_pct"	"$3"	"$4"	"$5"	"rrna_pct; for (i = 6; i<= NF; i++) {printf "	"$i}; print ""}' \
  metrics/trimSampleTable.tsv \
  report/trimAlignmentTable.tsv.tmp \
  > report/trimAlignmentTable.tsv
else
  cp report/trimAlignmentTable.tsv.tmp report/trimAlignmentTable.tsv
fi && \
rm report/trimAlignmentTable.tsv.tmp && \
trim_alignment_table_md=`if [[ -f metrics/trimSampleTable.tsv ]] ; then cut -f1-13 report/trimAlignmentTable.tsv | LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4), sprintf("%\47d", $5), sprintf("%.1f", $6), sprintf("%\47d", $7), sprintf("%.1f", $8), sprintf("%\47d", $9), sprintf("%.1f", $10), sprintf("%.2f", $11), sprintf("%.2f", $12), sprintf("%\47d", $13)}}' ; else cat report/trimAlignmentTable.tsv | LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:|-----:|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4), sprintf("%\47d", $5), sprintf("%.2f", $6), sprintf("%.2f", $7), $8}}' ; fi`
pandoc \
  /home/efournie/genpipes/bfx/report/RnaSeq.rnaseqc.md \
  --template /home/efournie/genpipes/bfx/report/RnaSeq.rnaseqc.md \
  --variable trim_alignment_table="$trim_alignment_table_md" \
  --to markdown \
  > report/RnaSeq.rnaseqc.md
rnaseqc_report.67821e702049231d38161c5db01ef648.mugqic.done
)
rnaseqc_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$rnaseqc_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: wiggle
#-------------------------------------------------------------------------------
STEP=wiggle
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: wiggle_1_JOB_ID: wiggle.Tx1_Days0_Rep1.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days0_Rep1.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days0_Rep1.forward.7b3588975367a23dd6b0b67d7909bf41.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days0_Rep1.forward.7b3588975367a23dd6b0b67d7909bf41.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days0_Rep1/Tx1_Days0_Rep1.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days0_Rep1/Tx1_Days0_Rep1.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days0_Rep1/Tx1_Days0_Rep1.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days0_Rep1.forward.bw
wiggle.Tx1_Days0_Rep1.forward.7b3588975367a23dd6b0b67d7909bf41.mugqic.done
)
wiggle_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_2_JOB_ID: wiggle.Tx1_Days0_Rep1.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days0_Rep1.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days0_Rep1.reverse.8c96003d24256236de56294a20186e86.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days0_Rep1.reverse.8c96003d24256236de56294a20186e86.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days0_Rep1/Tx1_Days0_Rep1.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days0_Rep1/Tx1_Days0_Rep1.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days0_Rep1/Tx1_Days0_Rep1.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days0_Rep1.reverse.bw
wiggle.Tx1_Days0_Rep1.reverse.8c96003d24256236de56294a20186e86.mugqic.done
)
wiggle_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_3_JOB_ID: wiggle.Tx1_Days0_Rep2.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days0_Rep2.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days0_Rep2.forward.47c108b7032f5739adc3fa73186052dc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days0_Rep2.forward.47c108b7032f5739adc3fa73186052dc.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days0_Rep2/Tx1_Days0_Rep2.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days0_Rep2/Tx1_Days0_Rep2.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days0_Rep2/Tx1_Days0_Rep2.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days0_Rep2.forward.bw
wiggle.Tx1_Days0_Rep2.forward.47c108b7032f5739adc3fa73186052dc.mugqic.done
)
wiggle_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_4_JOB_ID: wiggle.Tx1_Days0_Rep2.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days0_Rep2.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days0_Rep2.reverse.69c8151ebdffd68649dd62af288b9dbf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days0_Rep2.reverse.69c8151ebdffd68649dd62af288b9dbf.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days0_Rep2/Tx1_Days0_Rep2.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days0_Rep2/Tx1_Days0_Rep2.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days0_Rep2/Tx1_Days0_Rep2.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days0_Rep2.reverse.bw
wiggle.Tx1_Days0_Rep2.reverse.69c8151ebdffd68649dd62af288b9dbf.mugqic.done
)
wiggle_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_5_JOB_ID: wiggle.Tx1_Days0_Rep3.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days0_Rep3.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days0_Rep3.forward.be226100a1fda006f128921537cf95fd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days0_Rep3.forward.be226100a1fda006f128921537cf95fd.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days0_Rep3/Tx1_Days0_Rep3.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days0_Rep3/Tx1_Days0_Rep3.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days0_Rep3/Tx1_Days0_Rep3.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days0_Rep3.forward.bw
wiggle.Tx1_Days0_Rep3.forward.be226100a1fda006f128921537cf95fd.mugqic.done
)
wiggle_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_6_JOB_ID: wiggle.Tx1_Days0_Rep3.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days0_Rep3.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days0_Rep3.reverse.b9c7eb8f8350915bf87729d64f1b26f0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days0_Rep3.reverse.b9c7eb8f8350915bf87729d64f1b26f0.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days0_Rep3/Tx1_Days0_Rep3.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days0_Rep3/Tx1_Days0_Rep3.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days0_Rep3/Tx1_Days0_Rep3.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days0_Rep3.reverse.bw
wiggle.Tx1_Days0_Rep3.reverse.b9c7eb8f8350915bf87729d64f1b26f0.mugqic.done
)
wiggle_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_7_JOB_ID: wiggle.Tx1_Days0_Rep4.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days0_Rep4.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days0_Rep4.forward.e870cca039a311700fd392802de945f9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days0_Rep4.forward.e870cca039a311700fd392802de945f9.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days0_Rep4/Tx1_Days0_Rep4.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days0_Rep4/Tx1_Days0_Rep4.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days0_Rep4/Tx1_Days0_Rep4.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days0_Rep4.forward.bw
wiggle.Tx1_Days0_Rep4.forward.e870cca039a311700fd392802de945f9.mugqic.done
)
wiggle_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_8_JOB_ID: wiggle.Tx1_Days0_Rep4.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days0_Rep4.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days0_Rep4.reverse.eacc8394ac984d7a8743f2d6f13359ae.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days0_Rep4.reverse.eacc8394ac984d7a8743f2d6f13359ae.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days0_Rep4/Tx1_Days0_Rep4.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days0_Rep4/Tx1_Days0_Rep4.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days0_Rep4/Tx1_Days0_Rep4.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days0_Rep4.reverse.bw
wiggle.Tx1_Days0_Rep4.reverse.eacc8394ac984d7a8743f2d6f13359ae.mugqic.done
)
wiggle_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_9_JOB_ID: wiggle.Tx1_Days0_Rep5.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days0_Rep5.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days0_Rep5.forward.3265b3c2ea2738723a7a688321404855.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days0_Rep5.forward.3265b3c2ea2738723a7a688321404855.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days0_Rep5/Tx1_Days0_Rep5.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days0_Rep5/Tx1_Days0_Rep5.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days0_Rep5/Tx1_Days0_Rep5.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days0_Rep5.forward.bw
wiggle.Tx1_Days0_Rep5.forward.3265b3c2ea2738723a7a688321404855.mugqic.done
)
wiggle_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_10_JOB_ID: wiggle.Tx1_Days0_Rep5.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days0_Rep5.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days0_Rep5.reverse.46c44f0edb4fa5585f8cfa656c369212.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days0_Rep5.reverse.46c44f0edb4fa5585f8cfa656c369212.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days0_Rep5/Tx1_Days0_Rep5.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days0_Rep5/Tx1_Days0_Rep5.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days0_Rep5/Tx1_Days0_Rep5.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days0_Rep5.reverse.bw
wiggle.Tx1_Days0_Rep5.reverse.46c44f0edb4fa5585f8cfa656c369212.mugqic.done
)
wiggle_10_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_11_JOB_ID: wiggle.Tx1_Days0_Rep6.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days0_Rep6.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days0_Rep6.forward.c6a58602d797493e7db1dea28f6b1637.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days0_Rep6.forward.c6a58602d797493e7db1dea28f6b1637.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days0_Rep6/Tx1_Days0_Rep6.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days0_Rep6/Tx1_Days0_Rep6.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days0_Rep6/Tx1_Days0_Rep6.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days0_Rep6.forward.bw
wiggle.Tx1_Days0_Rep6.forward.c6a58602d797493e7db1dea28f6b1637.mugqic.done
)
wiggle_11_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_12_JOB_ID: wiggle.Tx1_Days0_Rep6.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days0_Rep6.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days0_Rep6.reverse.cdfa3663a9f4bf603fa74f7d12487b3f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days0_Rep6.reverse.cdfa3663a9f4bf603fa74f7d12487b3f.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days0_Rep6/Tx1_Days0_Rep6.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days0_Rep6/Tx1_Days0_Rep6.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days0_Rep6/Tx1_Days0_Rep6.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days0_Rep6.reverse.bw
wiggle.Tx1_Days0_Rep6.reverse.cdfa3663a9f4bf603fa74f7d12487b3f.mugqic.done
)
wiggle_12_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_13_JOB_ID: wiggle.Tx1_Days0_Rep7.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days0_Rep7.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days0_Rep7.forward.0696112f020217f12d295e05c8d7c938.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days0_Rep7.forward.0696112f020217f12d295e05c8d7c938.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days0_Rep7/Tx1_Days0_Rep7.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days0_Rep7/Tx1_Days0_Rep7.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days0_Rep7/Tx1_Days0_Rep7.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days0_Rep7.forward.bw
wiggle.Tx1_Days0_Rep7.forward.0696112f020217f12d295e05c8d7c938.mugqic.done
)
wiggle_13_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_14_JOB_ID: wiggle.Tx1_Days0_Rep7.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days0_Rep7.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days0_Rep7.reverse.db9e6d98d2a709871c588375899af9da.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days0_Rep7.reverse.db9e6d98d2a709871c588375899af9da.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days0_Rep7/Tx1_Days0_Rep7.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days0_Rep7/Tx1_Days0_Rep7.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days0_Rep7/Tx1_Days0_Rep7.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days0_Rep7.reverse.bw
wiggle.Tx1_Days0_Rep7.reverse.db9e6d98d2a709871c588375899af9da.mugqic.done
)
wiggle_14_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_15_JOB_ID: wiggle.Tx1_Days0_Rep8.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days0_Rep8.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days0_Rep8.forward.ae5906de97c86ee6b20e98052791efbf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days0_Rep8.forward.ae5906de97c86ee6b20e98052791efbf.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days0_Rep8/Tx1_Days0_Rep8.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days0_Rep8/Tx1_Days0_Rep8.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days0_Rep8/Tx1_Days0_Rep8.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days0_Rep8.forward.bw
wiggle.Tx1_Days0_Rep8.forward.ae5906de97c86ee6b20e98052791efbf.mugqic.done
)
wiggle_15_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_16_JOB_ID: wiggle.Tx1_Days0_Rep8.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days0_Rep8.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days0_Rep8.reverse.08ef699b006fb9b5c6e0cb79502edbb9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days0_Rep8.reverse.08ef699b006fb9b5c6e0cb79502edbb9.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days0_Rep8/Tx1_Days0_Rep8.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days0_Rep8/Tx1_Days0_Rep8.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days0_Rep8/Tx1_Days0_Rep8.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days0_Rep8.reverse.bw
wiggle.Tx1_Days0_Rep8.reverse.08ef699b006fb9b5c6e0cb79502edbb9.mugqic.done
)
wiggle_16_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_17_JOB_ID: wiggle.Tx1_Days2_Rep1.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days2_Rep1.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days2_Rep1.forward.15cbe0911c615c71ac915abfdeb179fe.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days2_Rep1.forward.15cbe0911c615c71ac915abfdeb179fe.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days2_Rep1/Tx1_Days2_Rep1.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days2_Rep1/Tx1_Days2_Rep1.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days2_Rep1/Tx1_Days2_Rep1.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days2_Rep1.forward.bw
wiggle.Tx1_Days2_Rep1.forward.15cbe0911c615c71ac915abfdeb179fe.mugqic.done
)
wiggle_17_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_17_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_18_JOB_ID: wiggle.Tx1_Days2_Rep1.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days2_Rep1.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days2_Rep1.reverse.d1190931fb5a1b9cffb696d40bc87a01.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days2_Rep1.reverse.d1190931fb5a1b9cffb696d40bc87a01.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days2_Rep1/Tx1_Days2_Rep1.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days2_Rep1/Tx1_Days2_Rep1.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days2_Rep1/Tx1_Days2_Rep1.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days2_Rep1.reverse.bw
wiggle.Tx1_Days2_Rep1.reverse.d1190931fb5a1b9cffb696d40bc87a01.mugqic.done
)
wiggle_18_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_18_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_19_JOB_ID: wiggle.Tx1_Days2_Rep2.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days2_Rep2.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days2_Rep2.forward.37e3e385350af4430d96544fd5fbc90e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days2_Rep2.forward.37e3e385350af4430d96544fd5fbc90e.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days2_Rep2/Tx1_Days2_Rep2.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days2_Rep2/Tx1_Days2_Rep2.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days2_Rep2/Tx1_Days2_Rep2.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days2_Rep2.forward.bw
wiggle.Tx1_Days2_Rep2.forward.37e3e385350af4430d96544fd5fbc90e.mugqic.done
)
wiggle_19_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_19_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_20_JOB_ID: wiggle.Tx1_Days2_Rep2.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days2_Rep2.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days2_Rep2.reverse.01ad9f10bd12a255d311971350328c80.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days2_Rep2.reverse.01ad9f10bd12a255d311971350328c80.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days2_Rep2/Tx1_Days2_Rep2.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days2_Rep2/Tx1_Days2_Rep2.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days2_Rep2/Tx1_Days2_Rep2.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days2_Rep2.reverse.bw
wiggle.Tx1_Days2_Rep2.reverse.01ad9f10bd12a255d311971350328c80.mugqic.done
)
wiggle_20_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_20_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_21_JOB_ID: wiggle.Tx1_Days2_Rep3.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days2_Rep3.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days2_Rep3.forward.4bdd817492d8c0de3088ba55bed750d0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days2_Rep3.forward.4bdd817492d8c0de3088ba55bed750d0.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days2_Rep3/Tx1_Days2_Rep3.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days2_Rep3/Tx1_Days2_Rep3.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days2_Rep3/Tx1_Days2_Rep3.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days2_Rep3.forward.bw
wiggle.Tx1_Days2_Rep3.forward.4bdd817492d8c0de3088ba55bed750d0.mugqic.done
)
wiggle_21_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_21_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_22_JOB_ID: wiggle.Tx1_Days2_Rep3.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days2_Rep3.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days2_Rep3.reverse.bd541cc54424f47feec039fe07a9a45c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days2_Rep3.reverse.bd541cc54424f47feec039fe07a9a45c.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days2_Rep3/Tx1_Days2_Rep3.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days2_Rep3/Tx1_Days2_Rep3.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days2_Rep3/Tx1_Days2_Rep3.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days2_Rep3.reverse.bw
wiggle.Tx1_Days2_Rep3.reverse.bd541cc54424f47feec039fe07a9a45c.mugqic.done
)
wiggle_22_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_22_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_23_JOB_ID: wiggle.Tx1_Days2_Rep4.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days2_Rep4.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days2_Rep4.forward.2fb97e60b63f195f0d004ebe1a3683cd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days2_Rep4.forward.2fb97e60b63f195f0d004ebe1a3683cd.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days2_Rep4/Tx1_Days2_Rep4.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days2_Rep4/Tx1_Days2_Rep4.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days2_Rep4/Tx1_Days2_Rep4.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days2_Rep4.forward.bw
wiggle.Tx1_Days2_Rep4.forward.2fb97e60b63f195f0d004ebe1a3683cd.mugqic.done
)
wiggle_23_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_23_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_24_JOB_ID: wiggle.Tx1_Days2_Rep4.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days2_Rep4.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days2_Rep4.reverse.410b947a970aeaa6fa12cc80b2c13b31.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days2_Rep4.reverse.410b947a970aeaa6fa12cc80b2c13b31.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days2_Rep4/Tx1_Days2_Rep4.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days2_Rep4/Tx1_Days2_Rep4.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days2_Rep4/Tx1_Days2_Rep4.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days2_Rep4.reverse.bw
wiggle.Tx1_Days2_Rep4.reverse.410b947a970aeaa6fa12cc80b2c13b31.mugqic.done
)
wiggle_24_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_24_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_25_JOB_ID: wiggle.Tx1_Days2_Rep5.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days2_Rep5.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days2_Rep5.forward.d7748945e1bb17bad7a5f3726b389bad.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days2_Rep5.forward.d7748945e1bb17bad7a5f3726b389bad.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days2_Rep5/Tx1_Days2_Rep5.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days2_Rep5/Tx1_Days2_Rep5.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days2_Rep5/Tx1_Days2_Rep5.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days2_Rep5.forward.bw
wiggle.Tx1_Days2_Rep5.forward.d7748945e1bb17bad7a5f3726b389bad.mugqic.done
)
wiggle_25_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_25_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_26_JOB_ID: wiggle.Tx1_Days2_Rep5.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days2_Rep5.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days2_Rep5.reverse.8de4cead994f9be7dfc52090b46091a0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days2_Rep5.reverse.8de4cead994f9be7dfc52090b46091a0.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days2_Rep5/Tx1_Days2_Rep5.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days2_Rep5/Tx1_Days2_Rep5.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days2_Rep5/Tx1_Days2_Rep5.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days2_Rep5.reverse.bw
wiggle.Tx1_Days2_Rep5.reverse.8de4cead994f9be7dfc52090b46091a0.mugqic.done
)
wiggle_26_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_26_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_27_JOB_ID: wiggle.Tx1_Days2_Rep6.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days2_Rep6.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days2_Rep6.forward.29dcc6d2ef70dc05c8b4bea5ad6813e5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days2_Rep6.forward.29dcc6d2ef70dc05c8b4bea5ad6813e5.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days2_Rep6/Tx1_Days2_Rep6.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days2_Rep6/Tx1_Days2_Rep6.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days2_Rep6/Tx1_Days2_Rep6.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days2_Rep6.forward.bw
wiggle.Tx1_Days2_Rep6.forward.29dcc6d2ef70dc05c8b4bea5ad6813e5.mugqic.done
)
wiggle_27_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_27_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_28_JOB_ID: wiggle.Tx1_Days2_Rep6.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days2_Rep6.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days2_Rep6.reverse.d378add1c698c6d62d9d931285670e43.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days2_Rep6.reverse.d378add1c698c6d62d9d931285670e43.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days2_Rep6/Tx1_Days2_Rep6.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days2_Rep6/Tx1_Days2_Rep6.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days2_Rep6/Tx1_Days2_Rep6.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days2_Rep6.reverse.bw
wiggle.Tx1_Days2_Rep6.reverse.d378add1c698c6d62d9d931285670e43.mugqic.done
)
wiggle_28_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_28_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_29_JOB_ID: wiggle.Tx1_Days2_Rep7.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days2_Rep7.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days2_Rep7.forward.8e8415efeb8cded0f05dc4d8f8911db2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days2_Rep7.forward.8e8415efeb8cded0f05dc4d8f8911db2.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days2_Rep7/Tx1_Days2_Rep7.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days2_Rep7/Tx1_Days2_Rep7.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days2_Rep7/Tx1_Days2_Rep7.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days2_Rep7.forward.bw
wiggle.Tx1_Days2_Rep7.forward.8e8415efeb8cded0f05dc4d8f8911db2.mugqic.done
)
wiggle_29_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_29_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_30_JOB_ID: wiggle.Tx1_Days2_Rep7.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days2_Rep7.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days2_Rep7.reverse.7398ead1387bd47c11ea6ecaafe9536d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days2_Rep7.reverse.7398ead1387bd47c11ea6ecaafe9536d.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days2_Rep7/Tx1_Days2_Rep7.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days2_Rep7/Tx1_Days2_Rep7.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days2_Rep7/Tx1_Days2_Rep7.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days2_Rep7.reverse.bw
wiggle.Tx1_Days2_Rep7.reverse.7398ead1387bd47c11ea6ecaafe9536d.mugqic.done
)
wiggle_30_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_30_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_31_JOB_ID: wiggle.Tx1_Days2_Rep8.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days2_Rep8.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days2_Rep8.forward.129683ff3697aa95e3cca9b2da85e626.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days2_Rep8.forward.129683ff3697aa95e3cca9b2da85e626.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days2_Rep8/Tx1_Days2_Rep8.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days2_Rep8/Tx1_Days2_Rep8.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days2_Rep8/Tx1_Days2_Rep8.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days2_Rep8.forward.bw
wiggle.Tx1_Days2_Rep8.forward.129683ff3697aa95e3cca9b2da85e626.mugqic.done
)
wiggle_31_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_31_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_32_JOB_ID: wiggle.Tx1_Days2_Rep8.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days2_Rep8.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days2_Rep8.reverse.80817c2e1ada4be419dc5a09bbf0f77e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days2_Rep8.reverse.80817c2e1ada4be419dc5a09bbf0f77e.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days2_Rep8/Tx1_Days2_Rep8.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days2_Rep8/Tx1_Days2_Rep8.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days2_Rep8/Tx1_Days2_Rep8.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days2_Rep8.reverse.bw
wiggle.Tx1_Days2_Rep8.reverse.80817c2e1ada4be419dc5a09bbf0f77e.mugqic.done
)
wiggle_32_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_32_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_33_JOB_ID: wiggle.Tx1_Days8_Rep1.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days8_Rep1.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days8_Rep1.forward.fe0fc0dafa0e049575b54ac8262a2fe7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days8_Rep1.forward.fe0fc0dafa0e049575b54ac8262a2fe7.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days8_Rep1/Tx1_Days8_Rep1.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days8_Rep1/Tx1_Days8_Rep1.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days8_Rep1/Tx1_Days8_Rep1.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days8_Rep1.forward.bw
wiggle.Tx1_Days8_Rep1.forward.fe0fc0dafa0e049575b54ac8262a2fe7.mugqic.done
)
wiggle_33_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_33_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_34_JOB_ID: wiggle.Tx1_Days8_Rep1.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days8_Rep1.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days8_Rep1.reverse.9780cd63525dc081df27172609d9966c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days8_Rep1.reverse.9780cd63525dc081df27172609d9966c.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days8_Rep1/Tx1_Days8_Rep1.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days8_Rep1/Tx1_Days8_Rep1.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days8_Rep1/Tx1_Days8_Rep1.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days8_Rep1.reverse.bw
wiggle.Tx1_Days8_Rep1.reverse.9780cd63525dc081df27172609d9966c.mugqic.done
)
wiggle_34_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_34_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_35_JOB_ID: wiggle.Tx1_Days8_Rep2.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days8_Rep2.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days8_Rep2.forward.9181faa92f00613662a68bb8c9aa3577.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days8_Rep2.forward.9181faa92f00613662a68bb8c9aa3577.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days8_Rep2/Tx1_Days8_Rep2.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days8_Rep2/Tx1_Days8_Rep2.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days8_Rep2/Tx1_Days8_Rep2.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days8_Rep2.forward.bw
wiggle.Tx1_Days8_Rep2.forward.9181faa92f00613662a68bb8c9aa3577.mugqic.done
)
wiggle_35_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_35_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_36_JOB_ID: wiggle.Tx1_Days8_Rep2.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days8_Rep2.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days8_Rep2.reverse.b140ac47eed411c3affea1b8535a7a0e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days8_Rep2.reverse.b140ac47eed411c3affea1b8535a7a0e.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days8_Rep2/Tx1_Days8_Rep2.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days8_Rep2/Tx1_Days8_Rep2.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days8_Rep2/Tx1_Days8_Rep2.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days8_Rep2.reverse.bw
wiggle.Tx1_Days8_Rep2.reverse.b140ac47eed411c3affea1b8535a7a0e.mugqic.done
)
wiggle_36_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_36_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_37_JOB_ID: wiggle.Tx1_Days8_Rep3.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days8_Rep3.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days8_Rep3.forward.88973958b714e7d3d040025054662919.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days8_Rep3.forward.88973958b714e7d3d040025054662919.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days8_Rep3/Tx1_Days8_Rep3.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days8_Rep3/Tx1_Days8_Rep3.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days8_Rep3/Tx1_Days8_Rep3.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days8_Rep3.forward.bw
wiggle.Tx1_Days8_Rep3.forward.88973958b714e7d3d040025054662919.mugqic.done
)
wiggle_37_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_37_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_38_JOB_ID: wiggle.Tx1_Days8_Rep3.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days8_Rep3.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days8_Rep3.reverse.fcf869634872859febda93f9eb15b86d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days8_Rep3.reverse.fcf869634872859febda93f9eb15b86d.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days8_Rep3/Tx1_Days8_Rep3.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days8_Rep3/Tx1_Days8_Rep3.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days8_Rep3/Tx1_Days8_Rep3.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days8_Rep3.reverse.bw
wiggle.Tx1_Days8_Rep3.reverse.fcf869634872859febda93f9eb15b86d.mugqic.done
)
wiggle_38_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_38_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_39_JOB_ID: wiggle.Tx1_Days8_Rep4.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days8_Rep4.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days8_Rep4.forward.a6db0e997b5f708272d26624a54a52af.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days8_Rep4.forward.a6db0e997b5f708272d26624a54a52af.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days8_Rep4/Tx1_Days8_Rep4.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days8_Rep4/Tx1_Days8_Rep4.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days8_Rep4/Tx1_Days8_Rep4.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days8_Rep4.forward.bw
wiggle.Tx1_Days8_Rep4.forward.a6db0e997b5f708272d26624a54a52af.mugqic.done
)
wiggle_39_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_39_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_40_JOB_ID: wiggle.Tx1_Days8_Rep4.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days8_Rep4.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days8_Rep4.reverse.46611e202b93b4ecc5f87d0e54047840.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days8_Rep4.reverse.46611e202b93b4ecc5f87d0e54047840.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days8_Rep4/Tx1_Days8_Rep4.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days8_Rep4/Tx1_Days8_Rep4.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days8_Rep4/Tx1_Days8_Rep4.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days8_Rep4.reverse.bw
wiggle.Tx1_Days8_Rep4.reverse.46611e202b93b4ecc5f87d0e54047840.mugqic.done
)
wiggle_40_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_40_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_41_JOB_ID: wiggle.Tx1_Days8_Rep5.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days8_Rep5.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days8_Rep5.forward.fa0b175993e57832a010a92349dd1ac8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days8_Rep5.forward.fa0b175993e57832a010a92349dd1ac8.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days8_Rep5/Tx1_Days8_Rep5.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days8_Rep5/Tx1_Days8_Rep5.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days8_Rep5/Tx1_Days8_Rep5.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days8_Rep5.forward.bw
wiggle.Tx1_Days8_Rep5.forward.fa0b175993e57832a010a92349dd1ac8.mugqic.done
)
wiggle_41_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_41_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_42_JOB_ID: wiggle.Tx1_Days8_Rep5.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days8_Rep5.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days8_Rep5.reverse.79d66617300af4425bc067e9840e3f90.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days8_Rep5.reverse.79d66617300af4425bc067e9840e3f90.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days8_Rep5/Tx1_Days8_Rep5.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days8_Rep5/Tx1_Days8_Rep5.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days8_Rep5/Tx1_Days8_Rep5.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days8_Rep5.reverse.bw
wiggle.Tx1_Days8_Rep5.reverse.79d66617300af4425bc067e9840e3f90.mugqic.done
)
wiggle_42_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_42_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_43_JOB_ID: wiggle.Tx1_Days8_Rep6.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days8_Rep6.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days8_Rep6.forward.978ca7e610705a881c617c08134ccc63.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days8_Rep6.forward.978ca7e610705a881c617c08134ccc63.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days8_Rep6/Tx1_Days8_Rep6.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days8_Rep6/Tx1_Days8_Rep6.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days8_Rep6/Tx1_Days8_Rep6.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days8_Rep6.forward.bw
wiggle.Tx1_Days8_Rep6.forward.978ca7e610705a881c617c08134ccc63.mugqic.done
)
wiggle_43_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_43_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_44_JOB_ID: wiggle.Tx1_Days8_Rep6.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days8_Rep6.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days8_Rep6.reverse.5e39c748b9e268ef88d162081709f0fa.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days8_Rep6.reverse.5e39c748b9e268ef88d162081709f0fa.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days8_Rep6/Tx1_Days8_Rep6.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days8_Rep6/Tx1_Days8_Rep6.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days8_Rep6/Tx1_Days8_Rep6.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days8_Rep6.reverse.bw
wiggle.Tx1_Days8_Rep6.reverse.5e39c748b9e268ef88d162081709f0fa.mugqic.done
)
wiggle_44_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_44_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_45_JOB_ID: wiggle.Tx1_Days8_Rep7.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days8_Rep7.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days8_Rep7.forward.0c02a5a6dda467fc8b210b1b014fdb69.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days8_Rep7.forward.0c02a5a6dda467fc8b210b1b014fdb69.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days8_Rep7/Tx1_Days8_Rep7.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days8_Rep7/Tx1_Days8_Rep7.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days8_Rep7/Tx1_Days8_Rep7.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days8_Rep7.forward.bw
wiggle.Tx1_Days8_Rep7.forward.0c02a5a6dda467fc8b210b1b014fdb69.mugqic.done
)
wiggle_45_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_45_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_46_JOB_ID: wiggle.Tx1_Days8_Rep7.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days8_Rep7.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days8_Rep7.reverse.37e814d9ef6e9df1caf2f413b8120609.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days8_Rep7.reverse.37e814d9ef6e9df1caf2f413b8120609.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days8_Rep7/Tx1_Days8_Rep7.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days8_Rep7/Tx1_Days8_Rep7.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days8_Rep7/Tx1_Days8_Rep7.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days8_Rep7.reverse.bw
wiggle.Tx1_Days8_Rep7.reverse.37e814d9ef6e9df1caf2f413b8120609.mugqic.done
)
wiggle_46_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_46_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_47_JOB_ID: wiggle.Tx1_Days8_Rep8.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days8_Rep8.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days8_Rep8.forward.c69b1e34c35f5b62764dbcd3180db3cf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days8_Rep8.forward.c69b1e34c35f5b62764dbcd3180db3cf.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days8_Rep8/Tx1_Days8_Rep8.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days8_Rep8/Tx1_Days8_Rep8.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days8_Rep8/Tx1_Days8_Rep8.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days8_Rep8.forward.bw
wiggle.Tx1_Days8_Rep8.forward.c69b1e34c35f5b62764dbcd3180db3cf.mugqic.done
)
wiggle_47_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_47_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_48_JOB_ID: wiggle.Tx1_Days8_Rep8.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx1_Days8_Rep8.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx1_Days8_Rep8.reverse.e8cc38030c8856db351295c33129a250.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx1_Days8_Rep8.reverse.e8cc38030c8856db351295c33129a250.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx1_Days8_Rep8/Tx1_Days8_Rep8.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx1_Days8_Rep8/Tx1_Days8_Rep8.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx1_Days8_Rep8/Tx1_Days8_Rep8.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx1_Days8_Rep8.reverse.bw
wiggle.Tx1_Days8_Rep8.reverse.e8cc38030c8856db351295c33129a250.mugqic.done
)
wiggle_48_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_48_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_49_JOB_ID: wiggle.Tx3_Days0_Rep1.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days0_Rep1.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days0_Rep1.forward.6e4503ece1acee4ff60c054e2e6aa340.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days0_Rep1.forward.6e4503ece1acee4ff60c054e2e6aa340.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days0_Rep1/Tx3_Days0_Rep1.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days0_Rep1/Tx3_Days0_Rep1.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days0_Rep1/Tx3_Days0_Rep1.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days0_Rep1.forward.bw
wiggle.Tx3_Days0_Rep1.forward.6e4503ece1acee4ff60c054e2e6aa340.mugqic.done
)
wiggle_49_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_49_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_50_JOB_ID: wiggle.Tx3_Days0_Rep1.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days0_Rep1.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days0_Rep1.reverse.bf4241da111a7684f1f0920fcf0d746b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days0_Rep1.reverse.bf4241da111a7684f1f0920fcf0d746b.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days0_Rep1/Tx3_Days0_Rep1.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days0_Rep1/Tx3_Days0_Rep1.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days0_Rep1/Tx3_Days0_Rep1.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days0_Rep1.reverse.bw
wiggle.Tx3_Days0_Rep1.reverse.bf4241da111a7684f1f0920fcf0d746b.mugqic.done
)
wiggle_50_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_50_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_51_JOB_ID: wiggle.Tx3_Days0_Rep2.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days0_Rep2.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days0_Rep2.forward.68aaed66b72283cd0af1a84804372fc8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days0_Rep2.forward.68aaed66b72283cd0af1a84804372fc8.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days0_Rep2/Tx3_Days0_Rep2.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days0_Rep2/Tx3_Days0_Rep2.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days0_Rep2/Tx3_Days0_Rep2.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days0_Rep2.forward.bw
wiggle.Tx3_Days0_Rep2.forward.68aaed66b72283cd0af1a84804372fc8.mugqic.done
)
wiggle_51_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_51_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_52_JOB_ID: wiggle.Tx3_Days0_Rep2.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days0_Rep2.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days0_Rep2.reverse.963c42f676af227c3997c853d4311e43.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days0_Rep2.reverse.963c42f676af227c3997c853d4311e43.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days0_Rep2/Tx3_Days0_Rep2.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days0_Rep2/Tx3_Days0_Rep2.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days0_Rep2/Tx3_Days0_Rep2.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days0_Rep2.reverse.bw
wiggle.Tx3_Days0_Rep2.reverse.963c42f676af227c3997c853d4311e43.mugqic.done
)
wiggle_52_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_52_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_53_JOB_ID: wiggle.Tx3_Days0_Rep3.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days0_Rep3.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days0_Rep3.forward.0a997917ac7140410285dbc90ca74b50.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days0_Rep3.forward.0a997917ac7140410285dbc90ca74b50.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days0_Rep3/Tx3_Days0_Rep3.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days0_Rep3/Tx3_Days0_Rep3.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days0_Rep3/Tx3_Days0_Rep3.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days0_Rep3.forward.bw
wiggle.Tx3_Days0_Rep3.forward.0a997917ac7140410285dbc90ca74b50.mugqic.done
)
wiggle_53_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_53_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_54_JOB_ID: wiggle.Tx3_Days0_Rep3.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days0_Rep3.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days0_Rep3.reverse.5979353b99d865d3e9eb835737f9a0f4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days0_Rep3.reverse.5979353b99d865d3e9eb835737f9a0f4.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days0_Rep3/Tx3_Days0_Rep3.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days0_Rep3/Tx3_Days0_Rep3.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days0_Rep3/Tx3_Days0_Rep3.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days0_Rep3.reverse.bw
wiggle.Tx3_Days0_Rep3.reverse.5979353b99d865d3e9eb835737f9a0f4.mugqic.done
)
wiggle_54_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_54_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_55_JOB_ID: wiggle.Tx3_Days0_Rep4.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days0_Rep4.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days0_Rep4.forward.5d68cab699622c4ca57ac2090cb5a960.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days0_Rep4.forward.5d68cab699622c4ca57ac2090cb5a960.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days0_Rep4/Tx3_Days0_Rep4.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days0_Rep4/Tx3_Days0_Rep4.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days0_Rep4/Tx3_Days0_Rep4.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days0_Rep4.forward.bw
wiggle.Tx3_Days0_Rep4.forward.5d68cab699622c4ca57ac2090cb5a960.mugqic.done
)
wiggle_55_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_55_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_56_JOB_ID: wiggle.Tx3_Days0_Rep4.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days0_Rep4.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days0_Rep4.reverse.c9925945335e667671dd50b30eaf7a69.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days0_Rep4.reverse.c9925945335e667671dd50b30eaf7a69.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days0_Rep4/Tx3_Days0_Rep4.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days0_Rep4/Tx3_Days0_Rep4.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days0_Rep4/Tx3_Days0_Rep4.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days0_Rep4.reverse.bw
wiggle.Tx3_Days0_Rep4.reverse.c9925945335e667671dd50b30eaf7a69.mugqic.done
)
wiggle_56_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_56_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_57_JOB_ID: wiggle.Tx3_Days0_Rep5.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days0_Rep5.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days0_Rep5.forward.382e782fca73fff0b4f39c7451cd8d1a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days0_Rep5.forward.382e782fca73fff0b4f39c7451cd8d1a.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days0_Rep5/Tx3_Days0_Rep5.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days0_Rep5/Tx3_Days0_Rep5.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days0_Rep5/Tx3_Days0_Rep5.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days0_Rep5.forward.bw
wiggle.Tx3_Days0_Rep5.forward.382e782fca73fff0b4f39c7451cd8d1a.mugqic.done
)
wiggle_57_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_57_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_58_JOB_ID: wiggle.Tx3_Days0_Rep5.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days0_Rep5.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days0_Rep5.reverse.a0761aefdd1fa752723bcf5a059e0e6d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days0_Rep5.reverse.a0761aefdd1fa752723bcf5a059e0e6d.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days0_Rep5/Tx3_Days0_Rep5.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days0_Rep5/Tx3_Days0_Rep5.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days0_Rep5/Tx3_Days0_Rep5.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days0_Rep5.reverse.bw
wiggle.Tx3_Days0_Rep5.reverse.a0761aefdd1fa752723bcf5a059e0e6d.mugqic.done
)
wiggle_58_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_58_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_59_JOB_ID: wiggle.Tx3_Days0_Rep6.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days0_Rep6.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days0_Rep6.forward.196ed22916d9ea63771259968bee5bae.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days0_Rep6.forward.196ed22916d9ea63771259968bee5bae.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days0_Rep6/Tx3_Days0_Rep6.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days0_Rep6/Tx3_Days0_Rep6.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days0_Rep6/Tx3_Days0_Rep6.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days0_Rep6.forward.bw
wiggle.Tx3_Days0_Rep6.forward.196ed22916d9ea63771259968bee5bae.mugqic.done
)
wiggle_59_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_59_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_60_JOB_ID: wiggle.Tx3_Days0_Rep6.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days0_Rep6.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days0_Rep6.reverse.648f3dd47c59ef4fc74a4d1962585972.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days0_Rep6.reverse.648f3dd47c59ef4fc74a4d1962585972.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days0_Rep6/Tx3_Days0_Rep6.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days0_Rep6/Tx3_Days0_Rep6.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days0_Rep6/Tx3_Days0_Rep6.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days0_Rep6.reverse.bw
wiggle.Tx3_Days0_Rep6.reverse.648f3dd47c59ef4fc74a4d1962585972.mugqic.done
)
wiggle_60_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_60_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_61_JOB_ID: wiggle.Tx3_Days0_Rep7.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days0_Rep7.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days0_Rep7.forward.386ab0c24a84b8e9341bfd4c4aaf3e96.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days0_Rep7.forward.386ab0c24a84b8e9341bfd4c4aaf3e96.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days0_Rep7/Tx3_Days0_Rep7.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days0_Rep7/Tx3_Days0_Rep7.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days0_Rep7/Tx3_Days0_Rep7.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days0_Rep7.forward.bw
wiggle.Tx3_Days0_Rep7.forward.386ab0c24a84b8e9341bfd4c4aaf3e96.mugqic.done
)
wiggle_61_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_61_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_62_JOB_ID: wiggle.Tx3_Days0_Rep7.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days0_Rep7.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days0_Rep7.reverse.7a9de3398c603204993972fb655f6cf9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days0_Rep7.reverse.7a9de3398c603204993972fb655f6cf9.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days0_Rep7/Tx3_Days0_Rep7.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days0_Rep7/Tx3_Days0_Rep7.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days0_Rep7/Tx3_Days0_Rep7.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days0_Rep7.reverse.bw
wiggle.Tx3_Days0_Rep7.reverse.7a9de3398c603204993972fb655f6cf9.mugqic.done
)
wiggle_62_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_62_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_63_JOB_ID: wiggle.Tx3_Days0_Rep8.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days0_Rep8.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days0_Rep8.forward.0a16bbf7ffc91b33254a8d5e56d30456.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days0_Rep8.forward.0a16bbf7ffc91b33254a8d5e56d30456.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days0_Rep8/Tx3_Days0_Rep8.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days0_Rep8/Tx3_Days0_Rep8.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days0_Rep8/Tx3_Days0_Rep8.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days0_Rep8.forward.bw
wiggle.Tx3_Days0_Rep8.forward.0a16bbf7ffc91b33254a8d5e56d30456.mugqic.done
)
wiggle_63_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_63_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_64_JOB_ID: wiggle.Tx3_Days0_Rep8.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days0_Rep8.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days0_Rep8.reverse.5df995e31afab8b8d75670e021232517.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days0_Rep8.reverse.5df995e31afab8b8d75670e021232517.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days0_Rep8/Tx3_Days0_Rep8.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days0_Rep8/Tx3_Days0_Rep8.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days0_Rep8/Tx3_Days0_Rep8.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days0_Rep8.reverse.bw
wiggle.Tx3_Days0_Rep8.reverse.5df995e31afab8b8d75670e021232517.mugqic.done
)
wiggle_64_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_64_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_65_JOB_ID: wiggle.Tx3_Days2_Rep1.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days2_Rep1.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days2_Rep1.forward.f1c55572e43a66bf47f9e49a7de31a34.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days2_Rep1.forward.f1c55572e43a66bf47f9e49a7de31a34.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days2_Rep1/Tx3_Days2_Rep1.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days2_Rep1/Tx3_Days2_Rep1.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days2_Rep1/Tx3_Days2_Rep1.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days2_Rep1.forward.bw
wiggle.Tx3_Days2_Rep1.forward.f1c55572e43a66bf47f9e49a7de31a34.mugqic.done
)
wiggle_65_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_65_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_66_JOB_ID: wiggle.Tx3_Days2_Rep1.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days2_Rep1.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days2_Rep1.reverse.2888c7005182ed3a3b14dc30cb59281a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days2_Rep1.reverse.2888c7005182ed3a3b14dc30cb59281a.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days2_Rep1/Tx3_Days2_Rep1.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days2_Rep1/Tx3_Days2_Rep1.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days2_Rep1/Tx3_Days2_Rep1.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days2_Rep1.reverse.bw
wiggle.Tx3_Days2_Rep1.reverse.2888c7005182ed3a3b14dc30cb59281a.mugqic.done
)
wiggle_66_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_66_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_67_JOB_ID: wiggle.Tx3_Days2_Rep2.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days2_Rep2.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days2_Rep2.forward.b568ea67535e9a5229c51e395ec24f09.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days2_Rep2.forward.b568ea67535e9a5229c51e395ec24f09.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days2_Rep2/Tx3_Days2_Rep2.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days2_Rep2/Tx3_Days2_Rep2.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days2_Rep2/Tx3_Days2_Rep2.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days2_Rep2.forward.bw
wiggle.Tx3_Days2_Rep2.forward.b568ea67535e9a5229c51e395ec24f09.mugqic.done
)
wiggle_67_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_67_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_68_JOB_ID: wiggle.Tx3_Days2_Rep2.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days2_Rep2.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days2_Rep2.reverse.136adfd67a548fe5ac153ee50b7a964c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days2_Rep2.reverse.136adfd67a548fe5ac153ee50b7a964c.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days2_Rep2/Tx3_Days2_Rep2.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days2_Rep2/Tx3_Days2_Rep2.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days2_Rep2/Tx3_Days2_Rep2.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days2_Rep2.reverse.bw
wiggle.Tx3_Days2_Rep2.reverse.136adfd67a548fe5ac153ee50b7a964c.mugqic.done
)
wiggle_68_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_68_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_69_JOB_ID: wiggle.Tx3_Days2_Rep3.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days2_Rep3.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days2_Rep3.forward.60c12f20a6ef956ab136d008c7abcdfb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days2_Rep3.forward.60c12f20a6ef956ab136d008c7abcdfb.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days2_Rep3/Tx3_Days2_Rep3.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days2_Rep3/Tx3_Days2_Rep3.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days2_Rep3/Tx3_Days2_Rep3.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days2_Rep3.forward.bw
wiggle.Tx3_Days2_Rep3.forward.60c12f20a6ef956ab136d008c7abcdfb.mugqic.done
)
wiggle_69_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_69_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_70_JOB_ID: wiggle.Tx3_Days2_Rep3.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days2_Rep3.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days2_Rep3.reverse.eb5a748a16d79b45856782d2a461a00a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days2_Rep3.reverse.eb5a748a16d79b45856782d2a461a00a.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days2_Rep3/Tx3_Days2_Rep3.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days2_Rep3/Tx3_Days2_Rep3.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days2_Rep3/Tx3_Days2_Rep3.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days2_Rep3.reverse.bw
wiggle.Tx3_Days2_Rep3.reverse.eb5a748a16d79b45856782d2a461a00a.mugqic.done
)
wiggle_70_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_70_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_71_JOB_ID: wiggle.Tx3_Days2_Rep4.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days2_Rep4.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days2_Rep4.forward.e51e2344ab32fbc53ac638d7f68c646f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days2_Rep4.forward.e51e2344ab32fbc53ac638d7f68c646f.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days2_Rep4/Tx3_Days2_Rep4.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days2_Rep4/Tx3_Days2_Rep4.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days2_Rep4/Tx3_Days2_Rep4.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days2_Rep4.forward.bw
wiggle.Tx3_Days2_Rep4.forward.e51e2344ab32fbc53ac638d7f68c646f.mugqic.done
)
wiggle_71_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_71_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_72_JOB_ID: wiggle.Tx3_Days2_Rep4.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days2_Rep4.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days2_Rep4.reverse.0282a8b404fe90867b57b483b0ae7f3d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days2_Rep4.reverse.0282a8b404fe90867b57b483b0ae7f3d.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days2_Rep4/Tx3_Days2_Rep4.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days2_Rep4/Tx3_Days2_Rep4.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days2_Rep4/Tx3_Days2_Rep4.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days2_Rep4.reverse.bw
wiggle.Tx3_Days2_Rep4.reverse.0282a8b404fe90867b57b483b0ae7f3d.mugqic.done
)
wiggle_72_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_72_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_73_JOB_ID: wiggle.Tx3_Days2_Rep5.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days2_Rep5.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days2_Rep5.forward.7e9a72c537a655cafca36dd92838c0dc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days2_Rep5.forward.7e9a72c537a655cafca36dd92838c0dc.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days2_Rep5/Tx3_Days2_Rep5.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days2_Rep5/Tx3_Days2_Rep5.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days2_Rep5/Tx3_Days2_Rep5.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days2_Rep5.forward.bw
wiggle.Tx3_Days2_Rep5.forward.7e9a72c537a655cafca36dd92838c0dc.mugqic.done
)
wiggle_73_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_73_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_74_JOB_ID: wiggle.Tx3_Days2_Rep5.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days2_Rep5.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days2_Rep5.reverse.2ecb9507d22d82b50bb258e3a3cdbd35.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days2_Rep5.reverse.2ecb9507d22d82b50bb258e3a3cdbd35.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days2_Rep5/Tx3_Days2_Rep5.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days2_Rep5/Tx3_Days2_Rep5.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days2_Rep5/Tx3_Days2_Rep5.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days2_Rep5.reverse.bw
wiggle.Tx3_Days2_Rep5.reverse.2ecb9507d22d82b50bb258e3a3cdbd35.mugqic.done
)
wiggle_74_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_74_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_75_JOB_ID: wiggle.Tx3_Days2_Rep6.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days2_Rep6.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days2_Rep6.forward.fd080391813cb854927724dbc9a13e39.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days2_Rep6.forward.fd080391813cb854927724dbc9a13e39.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days2_Rep6/Tx3_Days2_Rep6.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days2_Rep6/Tx3_Days2_Rep6.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days2_Rep6/Tx3_Days2_Rep6.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days2_Rep6.forward.bw
wiggle.Tx3_Days2_Rep6.forward.fd080391813cb854927724dbc9a13e39.mugqic.done
)
wiggle_75_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_75_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_76_JOB_ID: wiggle.Tx3_Days2_Rep6.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days2_Rep6.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days2_Rep6.reverse.291d548eca4cbdfedab5ae0a85d6549d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days2_Rep6.reverse.291d548eca4cbdfedab5ae0a85d6549d.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days2_Rep6/Tx3_Days2_Rep6.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days2_Rep6/Tx3_Days2_Rep6.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days2_Rep6/Tx3_Days2_Rep6.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days2_Rep6.reverse.bw
wiggle.Tx3_Days2_Rep6.reverse.291d548eca4cbdfedab5ae0a85d6549d.mugqic.done
)
wiggle_76_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_76_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_77_JOB_ID: wiggle.Tx3_Days2_Rep7.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days2_Rep7.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days2_Rep7.forward.f96e03e9c4911aec7267ca4092b940a1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days2_Rep7.forward.f96e03e9c4911aec7267ca4092b940a1.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days2_Rep7/Tx3_Days2_Rep7.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days2_Rep7/Tx3_Days2_Rep7.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days2_Rep7/Tx3_Days2_Rep7.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days2_Rep7.forward.bw
wiggle.Tx3_Days2_Rep7.forward.f96e03e9c4911aec7267ca4092b940a1.mugqic.done
)
wiggle_77_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_77_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_78_JOB_ID: wiggle.Tx3_Days2_Rep7.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days2_Rep7.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days2_Rep7.reverse.301250931995cd293fe229ef2f090f8d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days2_Rep7.reverse.301250931995cd293fe229ef2f090f8d.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days2_Rep7/Tx3_Days2_Rep7.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days2_Rep7/Tx3_Days2_Rep7.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days2_Rep7/Tx3_Days2_Rep7.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days2_Rep7.reverse.bw
wiggle.Tx3_Days2_Rep7.reverse.301250931995cd293fe229ef2f090f8d.mugqic.done
)
wiggle_78_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_78_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_79_JOB_ID: wiggle.Tx3_Days2_Rep8.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days2_Rep8.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days2_Rep8.forward.59e42e5fdaf994284d5d3da2e0ef09cf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days2_Rep8.forward.59e42e5fdaf994284d5d3da2e0ef09cf.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days2_Rep8/Tx3_Days2_Rep8.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days2_Rep8/Tx3_Days2_Rep8.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days2_Rep8/Tx3_Days2_Rep8.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days2_Rep8.forward.bw
wiggle.Tx3_Days2_Rep8.forward.59e42e5fdaf994284d5d3da2e0ef09cf.mugqic.done
)
wiggle_79_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_79_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_80_JOB_ID: wiggle.Tx3_Days2_Rep8.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days2_Rep8.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days2_Rep8.reverse.caafcf91d2b1199a294ca7c356a3732a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days2_Rep8.reverse.caafcf91d2b1199a294ca7c356a3732a.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days2_Rep8/Tx3_Days2_Rep8.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days2_Rep8/Tx3_Days2_Rep8.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days2_Rep8/Tx3_Days2_Rep8.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days2_Rep8.reverse.bw
wiggle.Tx3_Days2_Rep8.reverse.caafcf91d2b1199a294ca7c356a3732a.mugqic.done
)
wiggle_80_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_80_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_81_JOB_ID: wiggle.Tx3_Days8_Rep1.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days8_Rep1.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days8_Rep1.forward.14eadd5c468eb784b80a15b9b905df87.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days8_Rep1.forward.14eadd5c468eb784b80a15b9b905df87.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days8_Rep1/Tx3_Days8_Rep1.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days8_Rep1/Tx3_Days8_Rep1.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days8_Rep1/Tx3_Days8_Rep1.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days8_Rep1.forward.bw
wiggle.Tx3_Days8_Rep1.forward.14eadd5c468eb784b80a15b9b905df87.mugqic.done
)
wiggle_81_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_81_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_82_JOB_ID: wiggle.Tx3_Days8_Rep1.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days8_Rep1.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days8_Rep1.reverse.ab96912d81abab11d671479302460445.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days8_Rep1.reverse.ab96912d81abab11d671479302460445.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days8_Rep1/Tx3_Days8_Rep1.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days8_Rep1/Tx3_Days8_Rep1.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days8_Rep1/Tx3_Days8_Rep1.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days8_Rep1.reverse.bw
wiggle.Tx3_Days8_Rep1.reverse.ab96912d81abab11d671479302460445.mugqic.done
)
wiggle_82_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_82_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_83_JOB_ID: wiggle.Tx3_Days8_Rep2.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days8_Rep2.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days8_Rep2.forward.f39d7a7e7173c947907ed22f9ae19e3a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days8_Rep2.forward.f39d7a7e7173c947907ed22f9ae19e3a.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days8_Rep2/Tx3_Days8_Rep2.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days8_Rep2/Tx3_Days8_Rep2.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days8_Rep2/Tx3_Days8_Rep2.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days8_Rep2.forward.bw
wiggle.Tx3_Days8_Rep2.forward.f39d7a7e7173c947907ed22f9ae19e3a.mugqic.done
)
wiggle_83_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_83_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_84_JOB_ID: wiggle.Tx3_Days8_Rep2.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days8_Rep2.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days8_Rep2.reverse.e8a6dd0da1a9329abe238d102e654cf8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days8_Rep2.reverse.e8a6dd0da1a9329abe238d102e654cf8.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days8_Rep2/Tx3_Days8_Rep2.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days8_Rep2/Tx3_Days8_Rep2.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days8_Rep2/Tx3_Days8_Rep2.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days8_Rep2.reverse.bw
wiggle.Tx3_Days8_Rep2.reverse.e8a6dd0da1a9329abe238d102e654cf8.mugqic.done
)
wiggle_84_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_84_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_85_JOB_ID: wiggle.Tx3_Days8_Rep3.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days8_Rep3.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days8_Rep3.forward.2b1605fa187ebf8c4a52d428bb6f1f72.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days8_Rep3.forward.2b1605fa187ebf8c4a52d428bb6f1f72.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days8_Rep3/Tx3_Days8_Rep3.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days8_Rep3/Tx3_Days8_Rep3.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days8_Rep3/Tx3_Days8_Rep3.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days8_Rep3.forward.bw
wiggle.Tx3_Days8_Rep3.forward.2b1605fa187ebf8c4a52d428bb6f1f72.mugqic.done
)
wiggle_85_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_85_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_86_JOB_ID: wiggle.Tx3_Days8_Rep3.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days8_Rep3.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days8_Rep3.reverse.f46adea6e45e7b3346fe0b0fb130aa88.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days8_Rep3.reverse.f46adea6e45e7b3346fe0b0fb130aa88.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days8_Rep3/Tx3_Days8_Rep3.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days8_Rep3/Tx3_Days8_Rep3.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days8_Rep3/Tx3_Days8_Rep3.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days8_Rep3.reverse.bw
wiggle.Tx3_Days8_Rep3.reverse.f46adea6e45e7b3346fe0b0fb130aa88.mugqic.done
)
wiggle_86_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_86_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_87_JOB_ID: wiggle.Tx3_Days8_Rep4.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days8_Rep4.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days8_Rep4.forward.d195a3f5371256cac577c2b26b566f02.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days8_Rep4.forward.d195a3f5371256cac577c2b26b566f02.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days8_Rep4/Tx3_Days8_Rep4.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days8_Rep4/Tx3_Days8_Rep4.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days8_Rep4/Tx3_Days8_Rep4.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days8_Rep4.forward.bw
wiggle.Tx3_Days8_Rep4.forward.d195a3f5371256cac577c2b26b566f02.mugqic.done
)
wiggle_87_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_87_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_88_JOB_ID: wiggle.Tx3_Days8_Rep4.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days8_Rep4.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days8_Rep4.reverse.a9500d2d5845ec6d6b2eb3bb024ed872.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days8_Rep4.reverse.a9500d2d5845ec6d6b2eb3bb024ed872.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days8_Rep4/Tx3_Days8_Rep4.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days8_Rep4/Tx3_Days8_Rep4.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days8_Rep4/Tx3_Days8_Rep4.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days8_Rep4.reverse.bw
wiggle.Tx3_Days8_Rep4.reverse.a9500d2d5845ec6d6b2eb3bb024ed872.mugqic.done
)
wiggle_88_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_88_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_89_JOB_ID: wiggle.Tx3_Days8_Rep5.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days8_Rep5.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days8_Rep5.forward.b9de555990d40073e8e430ccc89afa98.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days8_Rep5.forward.b9de555990d40073e8e430ccc89afa98.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days8_Rep5/Tx3_Days8_Rep5.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days8_Rep5/Tx3_Days8_Rep5.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days8_Rep5/Tx3_Days8_Rep5.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days8_Rep5.forward.bw
wiggle.Tx3_Days8_Rep5.forward.b9de555990d40073e8e430ccc89afa98.mugqic.done
)
wiggle_89_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_89_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_90_JOB_ID: wiggle.Tx3_Days8_Rep5.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days8_Rep5.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days8_Rep5.reverse.124ee180db34e7ca1e9539e025afcb4c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days8_Rep5.reverse.124ee180db34e7ca1e9539e025afcb4c.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days8_Rep5/Tx3_Days8_Rep5.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days8_Rep5/Tx3_Days8_Rep5.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days8_Rep5/Tx3_Days8_Rep5.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days8_Rep5.reverse.bw
wiggle.Tx3_Days8_Rep5.reverse.124ee180db34e7ca1e9539e025afcb4c.mugqic.done
)
wiggle_90_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_90_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_91_JOB_ID: wiggle.Tx3_Days8_Rep6.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days8_Rep6.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days8_Rep6.forward.475819a4c8696d869f7484d397c51bc0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days8_Rep6.forward.475819a4c8696d869f7484d397c51bc0.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days8_Rep6/Tx3_Days8_Rep6.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days8_Rep6/Tx3_Days8_Rep6.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days8_Rep6/Tx3_Days8_Rep6.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days8_Rep6.forward.bw
wiggle.Tx3_Days8_Rep6.forward.475819a4c8696d869f7484d397c51bc0.mugqic.done
)
wiggle_91_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_91_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_92_JOB_ID: wiggle.Tx3_Days8_Rep6.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days8_Rep6.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days8_Rep6.reverse.f1dbd1bdfea2b4f7ff91318024b3f600.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days8_Rep6.reverse.f1dbd1bdfea2b4f7ff91318024b3f600.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days8_Rep6/Tx3_Days8_Rep6.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days8_Rep6/Tx3_Days8_Rep6.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days8_Rep6/Tx3_Days8_Rep6.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days8_Rep6.reverse.bw
wiggle.Tx3_Days8_Rep6.reverse.f1dbd1bdfea2b4f7ff91318024b3f600.mugqic.done
)
wiggle_92_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_92_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_93_JOB_ID: wiggle.Tx3_Days8_Rep7.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days8_Rep7.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days8_Rep7.forward.5bba98f5b84cdee1877dbaebc51c1cf7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days8_Rep7.forward.5bba98f5b84cdee1877dbaebc51c1cf7.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days8_Rep7/Tx3_Days8_Rep7.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days8_Rep7/Tx3_Days8_Rep7.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days8_Rep7/Tx3_Days8_Rep7.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days8_Rep7.forward.bw
wiggle.Tx3_Days8_Rep7.forward.5bba98f5b84cdee1877dbaebc51c1cf7.mugqic.done
)
wiggle_93_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_93_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_94_JOB_ID: wiggle.Tx3_Days8_Rep7.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days8_Rep7.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days8_Rep7.reverse.77ad5473f90426aef05b3c763b658974.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days8_Rep7.reverse.77ad5473f90426aef05b3c763b658974.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days8_Rep7/Tx3_Days8_Rep7.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days8_Rep7/Tx3_Days8_Rep7.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days8_Rep7/Tx3_Days8_Rep7.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days8_Rep7.reverse.bw
wiggle.Tx3_Days8_Rep7.reverse.77ad5473f90426aef05b3c763b658974.mugqic.done
)
wiggle_94_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_94_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_95_JOB_ID: wiggle.Tx3_Days8_Rep8.forward
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days8_Rep8.forward
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days8_Rep8.forward.6a16b9729aebc814ef562d2f154d9011.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days8_Rep8.forward.6a16b9729aebc814ef562d2f154d9011.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days8_Rep8/Tx3_Days8_Rep8.forward.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days8_Rep8/Tx3_Days8_Rep8.forward.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days8_Rep8/Tx3_Days8_Rep8.forward.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days8_Rep8.forward.bw
wiggle.Tx3_Days8_Rep8.forward.6a16b9729aebc814ef562d2f154d9011.mugqic.done
)
wiggle_95_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_95_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: wiggle_96_JOB_ID: wiggle.Tx3_Days8_Rep8.reverse
#-------------------------------------------------------------------------------
JOB_NAME=wiggle.Tx3_Days8_Rep8.reverse
JOB_DEPENDENCIES=
JOB_DONE=job_output/wiggle/wiggle.Tx3_Days8_Rep8.reverse.f02866e833920a9fbc6a5d6fea5b68d8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'wiggle.Tx3_Days8_Rep8.reverse.f02866e833920a9fbc6a5d6fea5b68d8.mugqic.done'
module load mugqic/ucsc/v346 && \
mkdir -p tracks/bigWig && \
cat tracks/Tx3_Days8_Rep8/Tx3_Days8_Rep8.reverse.bedGraph | sort --temporary-directory=${SLURM_TMPDIR} -k1,1 -k2,2n | \
awk '{if($0 !~ /^[A-W]/) print ""$0; else print $0}' | grep -vP "GL|lambda|pUC19" | sed 's/MT/chrM/' | \
awk '{printf "%s\t%d\t%d\t%4.4g\n", $1,$2,$3,$4}' > tracks/Tx3_Days8_Rep8/Tx3_Days8_Rep8.reverse.bedGraph.sorted && \
bedGraphToBigWig \
  tracks/Tx3_Days8_Rep8/Tx3_Days8_Rep8.reverse.bedGraph.sorted \
  /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.fai \
  tracks/bigWig/Tx3_Days8_Rep8.reverse.bw
wiggle.Tx3_Days8_Rep8.reverse.f02866e833920a9fbc6a5d6fea5b68d8.mugqic.done
)
wiggle_96_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"wiggle\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=12:00:0 --mem=48G -N 1 -n 12 | grep "[0-9]" | cut -d\  -f4)
echo "$wiggle_96_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: raw_counts_metrics
#-------------------------------------------------------------------------------
STEP=raw_counts_metrics
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: raw_counts_metrics_1_JOB_ID: metrics.wigzip
#-------------------------------------------------------------------------------
JOB_NAME=metrics.wigzip
JOB_DEPENDENCIES=$wiggle_1_JOB_ID:$wiggle_2_JOB_ID:$wiggle_3_JOB_ID:$wiggle_4_JOB_ID:$wiggle_5_JOB_ID:$wiggle_6_JOB_ID:$wiggle_7_JOB_ID:$wiggle_8_JOB_ID:$wiggle_9_JOB_ID:$wiggle_10_JOB_ID:$wiggle_11_JOB_ID:$wiggle_12_JOB_ID:$wiggle_13_JOB_ID:$wiggle_14_JOB_ID:$wiggle_15_JOB_ID:$wiggle_16_JOB_ID:$wiggle_17_JOB_ID:$wiggle_18_JOB_ID:$wiggle_19_JOB_ID:$wiggle_20_JOB_ID:$wiggle_21_JOB_ID:$wiggle_22_JOB_ID:$wiggle_23_JOB_ID:$wiggle_24_JOB_ID:$wiggle_25_JOB_ID:$wiggle_26_JOB_ID:$wiggle_27_JOB_ID:$wiggle_28_JOB_ID:$wiggle_29_JOB_ID:$wiggle_30_JOB_ID:$wiggle_31_JOB_ID:$wiggle_32_JOB_ID:$wiggle_33_JOB_ID:$wiggle_34_JOB_ID:$wiggle_35_JOB_ID:$wiggle_36_JOB_ID:$wiggle_37_JOB_ID:$wiggle_38_JOB_ID:$wiggle_39_JOB_ID:$wiggle_40_JOB_ID:$wiggle_41_JOB_ID:$wiggle_42_JOB_ID:$wiggle_43_JOB_ID:$wiggle_44_JOB_ID:$wiggle_45_JOB_ID:$wiggle_46_JOB_ID:$wiggle_47_JOB_ID:$wiggle_48_JOB_ID:$wiggle_49_JOB_ID:$wiggle_50_JOB_ID
JOB_DEPENDENCIES=$JOB_DEPENDENCIES:$wiggle_51_JOB_ID:$wiggle_52_JOB_ID:$wiggle_53_JOB_ID:$wiggle_54_JOB_ID:$wiggle_55_JOB_ID:$wiggle_56_JOB_ID:$wiggle_57_JOB_ID:$wiggle_58_JOB_ID:$wiggle_59_JOB_ID:$wiggle_60_JOB_ID:$wiggle_61_JOB_ID:$wiggle_62_JOB_ID:$wiggle_63_JOB_ID:$wiggle_64_JOB_ID:$wiggle_65_JOB_ID:$wiggle_66_JOB_ID:$wiggle_67_JOB_ID:$wiggle_68_JOB_ID:$wiggle_69_JOB_ID:$wiggle_70_JOB_ID:$wiggle_71_JOB_ID:$wiggle_72_JOB_ID:$wiggle_73_JOB_ID:$wiggle_74_JOB_ID:$wiggle_75_JOB_ID:$wiggle_76_JOB_ID:$wiggle_77_JOB_ID:$wiggle_78_JOB_ID:$wiggle_79_JOB_ID:$wiggle_80_JOB_ID:$wiggle_81_JOB_ID:$wiggle_82_JOB_ID:$wiggle_83_JOB_ID:$wiggle_84_JOB_ID:$wiggle_85_JOB_ID:$wiggle_86_JOB_ID:$wiggle_87_JOB_ID:$wiggle_88_JOB_ID:$wiggle_89_JOB_ID:$wiggle_90_JOB_ID:$wiggle_91_JOB_ID:$wiggle_92_JOB_ID:$wiggle_93_JOB_ID:$wiggle_94_JOB_ID:$wiggle_95_JOB_ID:$wiggle_96_JOB_ID
JOB_DONE=job_output/raw_counts_metrics/metrics.wigzip.edc4e268c60b94d072db60163e113f9c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'metrics.wigzip.edc4e268c60b94d072db60163e113f9c.mugqic.done'
zip -r tracks.zip tracks/bigWig
metrics.wigzip.edc4e268c60b94d072db60163e113f9c.mugqic.done
)
raw_counts_metrics_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=5:00:0 --mem=4G -N 1 -n 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_metrics_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_metrics_2_JOB_ID: rpkm_saturation
#-------------------------------------------------------------------------------
JOB_NAME=rpkm_saturation
JOB_DEPENDENCIES=
JOB_DONE=job_output/raw_counts_metrics/rpkm_saturation.c604b4e6f3dd7dd7d6547ef157df2ab9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'rpkm_saturation.c604b4e6f3dd7dd7d6547ef157df2ab9.mugqic.done'
module load mugqic/R_Bioconductor/3.4.2_3.6 mugqic/mugqic_tools/2.1.9 && \
mkdir -p metrics/saturation && \
Rscript $R_TOOLS/rpkmSaturation.R \
  DGE/rawCountMatrix.csv \
  /scratch/efournie/YM_rnaseq/ref/annotations/Sus_scrofa.Sscrofa11.1.Ensembl93.genes.length.tsv \
  raw_counts \
  metrics/saturation \
  15 \
  1 && \
zip -r metrics/saturation.zip metrics/saturation
rpkm_saturation.c604b4e6f3dd7dd7d6547ef157df2ab9.mugqic.done
)
raw_counts_metrics_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"raw_counts_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"raw_counts_metrics\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:0 --mem=128G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_metrics_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: raw_counts_metrics_3_JOB_ID: raw_count_metrics_report
#-------------------------------------------------------------------------------
JOB_NAME=raw_count_metrics_report
JOB_DEPENDENCIES=$rnaseqc_1_JOB_ID:$raw_counts_metrics_1_JOB_ID:$raw_counts_metrics_2_JOB_ID
JOB_DONE=job_output/raw_counts_metrics/raw_count_metrics_report.e4faf9a0ac8b5cf62e4257c28ff6a716.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'raw_count_metrics_report.e4faf9a0ac8b5cf62e4257c28ff6a716.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
cp metrics/rnaseqRep/corrMatrixSpearman.txt report/corrMatrixSpearman.tsv && \
cp tracks.zip report/ && \
cp metrics/saturation.zip report/ && \
pandoc --to=markdown \
  --template /home/efournie/genpipes/bfx/report/RnaSeq.raw_counts_metrics.md \
  --variable corr_matrix_spearman_table="`head -16 report/corrMatrixSpearman.tsv | cut -f-16| awk -F"	" '{OFS="	"; if (NR==1) {$0="Vs"$0; print; gsub(/[^	]/, "-"); print} else {printf $1; for (i=2; i<=NF; i++) {printf "	"sprintf("%.2f", $i)}; print ""}}' | sed 's/	/|/g'`" \
  /home/efournie/genpipes/bfx/report/RnaSeq.raw_counts_metrics.md \
  > report/RnaSeq.raw_counts_metrics.md
raw_count_metrics_report.e4faf9a0ac8b5cf62e4257c28ff6a716.mugqic.done
)
raw_counts_metrics_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE &&  $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE

if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=24:00:00 --mem=4G -n 1 -N 1 --depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]" | cut -d\  -f4)
echo "$raw_counts_metrics_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# STEP: cuffdiff
#-------------------------------------------------------------------------------
STEP=cuffdiff
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: cuffdiff_1_JOB_ID: cuffdiff.Tx3_vs_Tx1_at_Day0
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.Tx3_vs_Tx1_at_Day0
JOB_DEPENDENCIES=
JOB_DONE=job_output/cuffdiff/cuffdiff.Tx3_vs_Tx1_at_Day0.334c0fe7556a7776cb2655e713ce85dd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.Tx3_vs_Tx1_at_Day0.334c0fe7556a7776cb2655e713ce85dd.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/Tx3_vs_Tx1_at_Day0 && \
cuffdiff -u \
  --frag-bias-correct /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/Tx3_vs_Tx1_at_Day0 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/Tx1_Days0_Rep1/abundances.cxb,cufflinks/Tx1_Days0_Rep2/abundances.cxb,cufflinks/Tx1_Days0_Rep3/abundances.cxb,cufflinks/Tx1_Days0_Rep4/abundances.cxb,cufflinks/Tx1_Days0_Rep5/abundances.cxb,cufflinks/Tx1_Days0_Rep6/abundances.cxb,cufflinks/Tx1_Days0_Rep7/abundances.cxb,cufflinks/Tx1_Days0_Rep8/abundances.cxb \
  cufflinks/Tx3_Days0_Rep1/abundances.cxb,cufflinks/Tx3_Days0_Rep2/abundances.cxb,cufflinks/Tx3_Days0_Rep3/abundances.cxb,cufflinks/Tx3_Days0_Rep4/abundances.cxb,cufflinks/Tx3_Days0_Rep5/abundances.cxb,cufflinks/Tx3_Days0_Rep6/abundances.cxb,cufflinks/Tx3_Days0_Rep7/abundances.cxb,cufflinks/Tx3_Days0_Rep8/abundances.cxb
cuffdiff.Tx3_vs_Tx1_at_Day0.334c0fe7556a7776cb2655e713ce85dd.mugqic.done
)
cuffdiff_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffdiff_2_JOB_ID: cuffdiff.Tx3_vs_Tx1_at_Day2
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.Tx3_vs_Tx1_at_Day2
JOB_DEPENDENCIES=
JOB_DONE=job_output/cuffdiff/cuffdiff.Tx3_vs_Tx1_at_Day2.071d8dc5e7e9a419b55555d9432a36e6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.Tx3_vs_Tx1_at_Day2.071d8dc5e7e9a419b55555d9432a36e6.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/Tx3_vs_Tx1_at_Day2 && \
cuffdiff -u \
  --frag-bias-correct /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/Tx3_vs_Tx1_at_Day2 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/Tx1_Days2_Rep1/abundances.cxb,cufflinks/Tx1_Days2_Rep2/abundances.cxb,cufflinks/Tx1_Days2_Rep3/abundances.cxb,cufflinks/Tx1_Days2_Rep4/abundances.cxb,cufflinks/Tx1_Days2_Rep5/abundances.cxb,cufflinks/Tx1_Days2_Rep6/abundances.cxb,cufflinks/Tx1_Days2_Rep7/abundances.cxb,cufflinks/Tx1_Days2_Rep8/abundances.cxb \
  cufflinks/Tx3_Days2_Rep1/abundances.cxb,cufflinks/Tx3_Days2_Rep2/abundances.cxb,cufflinks/Tx3_Days2_Rep3/abundances.cxb,cufflinks/Tx3_Days2_Rep4/abundances.cxb,cufflinks/Tx3_Days2_Rep5/abundances.cxb,cufflinks/Tx3_Days2_Rep6/abundances.cxb,cufflinks/Tx3_Days2_Rep7/abundances.cxb,cufflinks/Tx3_Days2_Rep8/abundances.cxb
cuffdiff.Tx3_vs_Tx1_at_Day2.071d8dc5e7e9a419b55555d9432a36e6.mugqic.done
)
cuffdiff_2_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffdiff_3_JOB_ID: cuffdiff.Tx3_vs_Tx1_at_Day8
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.Tx3_vs_Tx1_at_Day8
JOB_DEPENDENCIES=
JOB_DONE=job_output/cuffdiff/cuffdiff.Tx3_vs_Tx1_at_Day8.3038ba0c9c880aefc1e84c3ec9e270d9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.Tx3_vs_Tx1_at_Day8.3038ba0c9c880aefc1e84c3ec9e270d9.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/Tx3_vs_Tx1_at_Day8 && \
cuffdiff -u \
  --frag-bias-correct /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/Tx3_vs_Tx1_at_Day8 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/Tx1_Days8_Rep1/abundances.cxb,cufflinks/Tx1_Days8_Rep2/abundances.cxb,cufflinks/Tx1_Days8_Rep3/abundances.cxb,cufflinks/Tx1_Days8_Rep4/abundances.cxb,cufflinks/Tx1_Days8_Rep5/abundances.cxb,cufflinks/Tx1_Days8_Rep6/abundances.cxb,cufflinks/Tx1_Days8_Rep7/abundances.cxb,cufflinks/Tx1_Days8_Rep8/abundances.cxb \
  cufflinks/Tx3_Days8_Rep1/abundances.cxb,cufflinks/Tx3_Days8_Rep2/abundances.cxb,cufflinks/Tx3_Days8_Rep3/abundances.cxb,cufflinks/Tx3_Days8_Rep4/abundances.cxb,cufflinks/Tx3_Days8_Rep5/abundances.cxb,cufflinks/Tx3_Days8_Rep6/abundances.cxb,cufflinks/Tx3_Days8_Rep7/abundances.cxb,cufflinks/Tx3_Days8_Rep8/abundances.cxb
cuffdiff.Tx3_vs_Tx1_at_Day8.3038ba0c9c880aefc1e84c3ec9e270d9.mugqic.done
)
cuffdiff_3_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffdiff_4_JOB_ID: cuffdiff.Day2_vs_Day0_Tx1
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.Day2_vs_Day0_Tx1
JOB_DEPENDENCIES=
JOB_DONE=job_output/cuffdiff/cuffdiff.Day2_vs_Day0_Tx1.772cb4a9cb6cff08113744b15a3aeeef.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.Day2_vs_Day0_Tx1.772cb4a9cb6cff08113744b15a3aeeef.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/Day2_vs_Day0_Tx1 && \
cuffdiff -u \
  --frag-bias-correct /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/Day2_vs_Day0_Tx1 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/Tx1_Days0_Rep1/abundances.cxb,cufflinks/Tx1_Days0_Rep2/abundances.cxb,cufflinks/Tx1_Days0_Rep3/abundances.cxb,cufflinks/Tx1_Days0_Rep4/abundances.cxb,cufflinks/Tx1_Days0_Rep5/abundances.cxb,cufflinks/Tx1_Days0_Rep6/abundances.cxb,cufflinks/Tx1_Days0_Rep7/abundances.cxb,cufflinks/Tx1_Days0_Rep8/abundances.cxb \
  cufflinks/Tx1_Days2_Rep1/abundances.cxb,cufflinks/Tx1_Days2_Rep2/abundances.cxb,cufflinks/Tx1_Days2_Rep3/abundances.cxb,cufflinks/Tx1_Days2_Rep4/abundances.cxb,cufflinks/Tx1_Days2_Rep5/abundances.cxb,cufflinks/Tx1_Days2_Rep6/abundances.cxb,cufflinks/Tx1_Days2_Rep7/abundances.cxb,cufflinks/Tx1_Days2_Rep8/abundances.cxb
cuffdiff.Day2_vs_Day0_Tx1.772cb4a9cb6cff08113744b15a3aeeef.mugqic.done
)
cuffdiff_4_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffdiff_5_JOB_ID: cuffdiff.Day8_vs_Day2_Tx1
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.Day8_vs_Day2_Tx1
JOB_DEPENDENCIES=
JOB_DONE=job_output/cuffdiff/cuffdiff.Day8_vs_Day2_Tx1.2098a82d3efbc9b46f1f5385d5e98235.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.Day8_vs_Day2_Tx1.2098a82d3efbc9b46f1f5385d5e98235.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/Day8_vs_Day2_Tx1 && \
cuffdiff -u \
  --frag-bias-correct /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/Day8_vs_Day2_Tx1 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/Tx1_Days2_Rep1/abundances.cxb,cufflinks/Tx1_Days2_Rep2/abundances.cxb,cufflinks/Tx1_Days2_Rep3/abundances.cxb,cufflinks/Tx1_Days2_Rep4/abundances.cxb,cufflinks/Tx1_Days2_Rep5/abundances.cxb,cufflinks/Tx1_Days2_Rep6/abundances.cxb,cufflinks/Tx1_Days2_Rep7/abundances.cxb,cufflinks/Tx1_Days2_Rep8/abundances.cxb \
  cufflinks/Tx1_Days8_Rep1/abundances.cxb,cufflinks/Tx1_Days8_Rep2/abundances.cxb,cufflinks/Tx1_Days8_Rep3/abundances.cxb,cufflinks/Tx1_Days8_Rep4/abundances.cxb,cufflinks/Tx1_Days8_Rep5/abundances.cxb,cufflinks/Tx1_Days8_Rep6/abundances.cxb,cufflinks/Tx1_Days8_Rep7/abundances.cxb,cufflinks/Tx1_Days8_Rep8/abundances.cxb
cuffdiff.Day8_vs_Day2_Tx1.2098a82d3efbc9b46f1f5385d5e98235.mugqic.done
)
cuffdiff_5_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffdiff_6_JOB_ID: cuffdiff.Day8_vs_Day0_Tx1
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.Day8_vs_Day0_Tx1
JOB_DEPENDENCIES=
JOB_DONE=job_output/cuffdiff/cuffdiff.Day8_vs_Day0_Tx1.6ce21ee76a3cfb2ba46ae6446b6fad34.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.Day8_vs_Day0_Tx1.6ce21ee76a3cfb2ba46ae6446b6fad34.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/Day8_vs_Day0_Tx1 && \
cuffdiff -u \
  --frag-bias-correct /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/Day8_vs_Day0_Tx1 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/Tx1_Days0_Rep1/abundances.cxb,cufflinks/Tx1_Days0_Rep2/abundances.cxb,cufflinks/Tx1_Days0_Rep3/abundances.cxb,cufflinks/Tx1_Days0_Rep4/abundances.cxb,cufflinks/Tx1_Days0_Rep5/abundances.cxb,cufflinks/Tx1_Days0_Rep6/abundances.cxb,cufflinks/Tx1_Days0_Rep7/abundances.cxb,cufflinks/Tx1_Days0_Rep8/abundances.cxb \
  cufflinks/Tx1_Days8_Rep1/abundances.cxb,cufflinks/Tx1_Days8_Rep2/abundances.cxb,cufflinks/Tx1_Days8_Rep3/abundances.cxb,cufflinks/Tx1_Days8_Rep4/abundances.cxb,cufflinks/Tx1_Days8_Rep5/abundances.cxb,cufflinks/Tx1_Days8_Rep6/abundances.cxb,cufflinks/Tx1_Days8_Rep7/abundances.cxb,cufflinks/Tx1_Days8_Rep8/abundances.cxb
cuffdiff.Day8_vs_Day0_Tx1.6ce21ee76a3cfb2ba46ae6446b6fad34.mugqic.done
)
cuffdiff_6_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffdiff_7_JOB_ID: cuffdiff.Day2_vs_Day0_Tx2
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.Day2_vs_Day0_Tx2
JOB_DEPENDENCIES=
JOB_DONE=job_output/cuffdiff/cuffdiff.Day2_vs_Day0_Tx2.e7ef9f9af4614b1971481cc96f89981b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.Day2_vs_Day0_Tx2.e7ef9f9af4614b1971481cc96f89981b.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/Day2_vs_Day0_Tx2 && \
cuffdiff -u \
  --frag-bias-correct /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/Day2_vs_Day0_Tx2 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/Tx3_Days0_Rep1/abundances.cxb,cufflinks/Tx3_Days0_Rep2/abundances.cxb,cufflinks/Tx3_Days0_Rep3/abundances.cxb,cufflinks/Tx3_Days0_Rep4/abundances.cxb,cufflinks/Tx3_Days0_Rep5/abundances.cxb,cufflinks/Tx3_Days0_Rep6/abundances.cxb,cufflinks/Tx3_Days0_Rep7/abundances.cxb,cufflinks/Tx3_Days0_Rep8/abundances.cxb \
  cufflinks/Tx3_Days2_Rep1/abundances.cxb,cufflinks/Tx3_Days2_Rep2/abundances.cxb,cufflinks/Tx3_Days2_Rep3/abundances.cxb,cufflinks/Tx3_Days2_Rep4/abundances.cxb,cufflinks/Tx3_Days2_Rep5/abundances.cxb,cufflinks/Tx3_Days2_Rep6/abundances.cxb,cufflinks/Tx3_Days2_Rep7/abundances.cxb,cufflinks/Tx3_Days2_Rep8/abundances.cxb
cuffdiff.Day2_vs_Day0_Tx2.e7ef9f9af4614b1971481cc96f89981b.mugqic.done
)
cuffdiff_7_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffdiff_8_JOB_ID: cuffdiff.Day8_vs_Day2_Tx2
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.Day8_vs_Day2_Tx2
JOB_DEPENDENCIES=
JOB_DONE=job_output/cuffdiff/cuffdiff.Day8_vs_Day2_Tx2.408875ff9330ae71e429d36b2fabba50.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.Day8_vs_Day2_Tx2.408875ff9330ae71e429d36b2fabba50.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/Day8_vs_Day2_Tx2 && \
cuffdiff -u \
  --frag-bias-correct /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/Day8_vs_Day2_Tx2 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/Tx3_Days2_Rep1/abundances.cxb,cufflinks/Tx3_Days2_Rep2/abundances.cxb,cufflinks/Tx3_Days2_Rep3/abundances.cxb,cufflinks/Tx3_Days2_Rep4/abundances.cxb,cufflinks/Tx3_Days2_Rep5/abundances.cxb,cufflinks/Tx3_Days2_Rep6/abundances.cxb,cufflinks/Tx3_Days2_Rep7/abundances.cxb,cufflinks/Tx3_Days2_Rep8/abundances.cxb \
  cufflinks/Tx3_Days8_Rep1/abundances.cxb,cufflinks/Tx3_Days8_Rep2/abundances.cxb,cufflinks/Tx3_Days8_Rep3/abundances.cxb,cufflinks/Tx3_Days8_Rep4/abundances.cxb,cufflinks/Tx3_Days8_Rep5/abundances.cxb,cufflinks/Tx3_Days8_Rep6/abundances.cxb,cufflinks/Tx3_Days8_Rep7/abundances.cxb,cufflinks/Tx3_Days8_Rep8/abundances.cxb
cuffdiff.Day8_vs_Day2_Tx2.408875ff9330ae71e429d36b2fabba50.mugqic.done
)
cuffdiff_8_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# JOB: cuffdiff_9_JOB_ID: cuffdiff.Day8_vs_Day0_Tx2
#-------------------------------------------------------------------------------
JOB_NAME=cuffdiff.Day8_vs_Day0_Tx2
JOB_DEPENDENCIES=
JOB_DONE=job_output/cuffdiff/cuffdiff.Day8_vs_Day0_Tx2.0182f6676564da3e13d0fe1fa0f83e78.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'cuffdiff.Day8_vs_Day0_Tx2.0182f6676564da3e13d0fe1fa0f83e78.mugqic.done'
module load mugqic/cufflinks/2.2.1 && \
mkdir -p cuffdiff/Day8_vs_Day0_Tx2 && \
cuffdiff -u \
  --frag-bias-correct /scratch/efournie/YM_rnaseq/ref/genome/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
  --library-type fr-firststrand \
  --output-dir cuffdiff/Day8_vs_Day0_Tx2 \
  --num-threads 8 \
  cufflinks/AllSamples/merged.gtf \
  cufflinks/Tx3_Days0_Rep1/abundances.cxb,cufflinks/Tx3_Days0_Rep2/abundances.cxb,cufflinks/Tx3_Days0_Rep3/abundances.cxb,cufflinks/Tx3_Days0_Rep4/abundances.cxb,cufflinks/Tx3_Days0_Rep5/abundances.cxb,cufflinks/Tx3_Days0_Rep6/abundances.cxb,cufflinks/Tx3_Days0_Rep7/abundances.cxb,cufflinks/Tx3_Days0_Rep8/abundances.cxb \
  cufflinks/Tx3_Days8_Rep1/abundances.cxb,cufflinks/Tx3_Days8_Rep2/abundances.cxb,cufflinks/Tx3_Days8_Rep3/abundances.cxb,cufflinks/Tx3_Days8_Rep4/abundances.cxb,cufflinks/Tx3_Days8_Rep5/abundances.cxb,cufflinks/Tx3_Days8_Rep6/abundances.cxb,cufflinks/Tx3_Days8_Rep7/abundances.cxb,cufflinks/Tx3_Days8_Rep8/abundances.cxb
cuffdiff.Day8_vs_Day0_Tx2.0182f6676564da3e13d0fe1fa0f83e78.mugqic.done
)
cuffdiff_9_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"cuffdiff\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=48:00:0 --mem=32G -N 1 -n 8 | grep "[0-9]" | cut -d\  -f4)
echo "$cuffdiff_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# Call home with pipeline statistics
#-------------------------------------------------------------------------------
wget "http://mugqic.hpc.mcgill.ca/cgi-bin/pipeline.cgi?hostname=cedar5.cedar.computecanada.ca&ip=206.12.124.6&pipeline=RnaSeq&steps=picard_sam_to_fastq,trimmomatic,merge_trimmomatic_stats,star,picard_merge_sam_files,picard_sort_sam,picard_mark_duplicates,picard_rna_metrics,estimate_ribosomal_rna,bam_hard_clip,rnaseqc,wiggle,raw_counts,raw_counts_metrics,cufflinks,cuffmerge,cuffquant,cuffdiff,cuffnorm,fpkm_correlation_matrix&samples=48&AnonymizedList=e748dfbd05837dcacb6baee439895cb2,e4e24bf930011ab02ba083e2e7c95668,1d4d8e6150f59786603494635ec4621f,3c1539fc206f39b9a1ca4ac3e061c5b8,c324de5f7e4ff1851ec78395ea09686d,8f8fe834d81c82537c8bfd1d416ffbb5,56477db8a7f4bbd2de7aec57fd8b3699,7db6f8dee1143957d366f08040eb9afe,e3206a361002669fcd216de3c044ec5d,9227cf63bcfb6d30e5c99b7c85089b42,506bff58733f77f5656a57844ab90ab6,24ea019290e5b125d37feaa1a1fea88a,47368a5df0083a845cc4c93319587f9e,c22d8dc1f5bcb0713d947f4e112f6d4e,176b33fc60612097e5c09cfd1f259cfe,b287c88aa571b90a6153d267cdf43b78,ffb299d88d9602141ec92101bec16ed0,300476c6d4c94a0d37def774af0efbef,6618cc700aab3f5d7bddfd3928a7f35f,e0a2efd2d562ee5006d35a9a76aba0cd,0a2f60dbe954559b5b0bc895b179db77,b66ccd77145bdf23ecee9ea994f1e576,f48d5ecdff240be34a39857004189943,1ca64b01ad5ea9eec38382c6e3709bf1,16f57e2824c90ac5b0921830d9ac09dc,aae9a73bf9d2c59108e8df220c6340f4,c0084c81b975cebd8c69d20c1db282ec,913c8afc960193c6db4b0ebdb1d85e06,6fcc3c0d68bcbf5ea824af5019dd2177,60ebe8f518bc06a48f95c80aa9ba423e,efcf367e7abfcc1209426929813619b1,f5a6f1da2a39c678b30d794e83715d22,2c63dc92e6f0d5ebd4cf9221b2c0f45f,5496e882241e49079adf018aeedbe7e2,41b095d205527a3ddd468c065e663e13,ea92192db8bc850bf13f6201cb44abb4,c462dff9b138c9193a4ffa7dd37c19e4,ae911e39b4433e356139460a4830ec99,a1009d2cf123d3890ad85849f3a7ca5c,f3ebce6b41f431c88d948f7ffced7c4c,fe4684e329181a40a569f49e4340d8ed,1a7906dd093088c54d6ed3b5186b0cd5,2dc53280a0fc9d31202fc2e83e5e5985,f54f139b7d0802b2ef2ee21497161296,892bd5747cbe4ab4364085fae0122ec3,53bfadfb9624da2a08777cba4fa956c7,ab8d654bd2d1c788def8eff6c027cddc,d109f9717a454e04231cecaf160fb0a3" --quiet --output-document=/dev/null

