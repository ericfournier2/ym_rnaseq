mkdir ref
cd ref
wget ftp://ftp.ensembl.org/pub/release-93/fasta/sus_scrofa/dna/Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.gz
gunzip Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa.gz

module load samtools
samtools faidx Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa

mkdir annotations
wget ftp://ftp.ensembl.org/pub/release-93/gtf/sus_scrofa/Sus_scrofa.Sscrofa11.1.93.gtf.gz
gunzip Sus_scrofa.Sscrofa11.1.93.gtf.gz
mv Sus_scrofa.Sscrofa11.1.93.gtf annotations/

module load star/2.6.1a
mkdir STAR_2.6.1a_index
STAR --runThreadN 8 \
     --runMode genomeGenerate \
     --genomeDir STAR_2.6.1a_index \
     --genomeFastaFiles Sus_scrofa.Sscrofa11.1.dna_sm.toplevel.fa \
     --sjdbGTFfile ../annotations/Sus_scrofa.Sscrofa11.1.93.gtf \
     --sjdbOverhang 100
     