#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#-------------------------------------------------------------------------------
# RnaSeq SlurmScheduler Job Submission Bash script
# Version: 3.0.1-beta
# Created on: 2018-11-03T21:18:50
# Steps:
#   differential_expression: 1 job
#   TOTAL: 1 job
#-------------------------------------------------------------------------------

OUTPUT_DIR=/scratch/efournie/YM_rnaseq/output/pipeline
JOB_OUTPUT_DIR=$OUTPUT_DIR/job_output
TIMESTAMP=`date +%FT%H.%M.%S`
JOB_LIST=$JOB_OUTPUT_DIR/RnaSeq_job_list_$TIMESTAMP
export CONFIG_FILES="/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini"
mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR


#-------------------------------------------------------------------------------
# STEP: differential_expression
#-------------------------------------------------------------------------------
STEP=differential_expression
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: differential_expression_1_JOB_ID: differential_expression
#-------------------------------------------------------------------------------
JOB_NAME=differential_expression
JOB_DEPENDENCIES=
JOB_DONE=job_output/differential_expression/differential_expression.c4f6a0baac3ce3a06446b0339fab650d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'differential_expression.c4f6a0baac3ce3a06446b0339fab650d.mugqic.done'
module load mugqic/mugqic_tools/2.1.9 mugqic/R_Bioconductor/3.4.2_3.6 && \
mkdir -p DGE && \
Rscript $R_TOOLS/edger.R \
  -d ../../raw/design.txt \
  -c DGE/rawCountMatrix.csv \
  -o DGE && \
Rscript $R_TOOLS/deseq.R \
  -d ../../raw/design.txt \
  -c DGE/rawCountMatrix.csv \
  -o DGE \
  
differential_expression.c4f6a0baac3ce3a06446b0339fab650d.mugqic.done
)
differential_expression_1_JOB_ID=$(echo "#! /bin/bash 
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
rm -f $JOB_DONE && module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"differential_expression\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \"running\"
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
module load mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9
/home/efournie/genpipes/utils/job2json.py \
  -u \"efournie\" \
  -c \"/home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini,/home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini,ref/sus_scrofa.ini\" \
  -s \"differential_expression\" \
  -j \"$JOB_NAME\" \
  -d \"$JOB_DONE\" \
  -l \"$JOB_OUTPUT\" \
  -o \"/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days0_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days2_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx1_Days8_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days0_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days2_Rep8.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep1.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep2.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep3.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep4.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep5.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep6.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep7.json,/scratch/efournie/YM_rnaseq/output/pipeline/json/Tx3_Days8_Rep8.json\" \
  -f \$MUGQIC_STATE
module unload mugqic/python/2.7.13 mugqic/mugqic_tools/2.1.9 
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $OUTPUT_DIR -o $JOB_OUTPUT -J $JOB_NAME --time=10:00:0 --mem=4G -N 1 -n 1 | grep "[0-9]" | cut -d\  -f4)
echo "$differential_expression_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

sleep 0.5


#-------------------------------------------------------------------------------
# Call home with pipeline statistics
#-------------------------------------------------------------------------------
wget "http://mugqic.hpc.mcgill.ca/cgi-bin/pipeline.cgi?hostname=cedar1.cedar.computecanada.ca&ip=206.12.124.2&pipeline=RnaSeq&steps=differential_expression&samples=48&AnonymizedList=e748dfbd05837dcacb6baee439895cb2,e4e24bf930011ab02ba083e2e7c95668,1d4d8e6150f59786603494635ec4621f,3c1539fc206f39b9a1ca4ac3e061c5b8,c324de5f7e4ff1851ec78395ea09686d,8f8fe834d81c82537c8bfd1d416ffbb5,56477db8a7f4bbd2de7aec57fd8b3699,7db6f8dee1143957d366f08040eb9afe,e3206a361002669fcd216de3c044ec5d,9227cf63bcfb6d30e5c99b7c85089b42,506bff58733f77f5656a57844ab90ab6,24ea019290e5b125d37feaa1a1fea88a,47368a5df0083a845cc4c93319587f9e,c22d8dc1f5bcb0713d947f4e112f6d4e,176b33fc60612097e5c09cfd1f259cfe,b287c88aa571b90a6153d267cdf43b78,ffb299d88d9602141ec92101bec16ed0,300476c6d4c94a0d37def774af0efbef,6618cc700aab3f5d7bddfd3928a7f35f,e0a2efd2d562ee5006d35a9a76aba0cd,0a2f60dbe954559b5b0bc895b179db77,b66ccd77145bdf23ecee9ea994f1e576,f48d5ecdff240be34a39857004189943,1ca64b01ad5ea9eec38382c6e3709bf1,16f57e2824c90ac5b0921830d9ac09dc,aae9a73bf9d2c59108e8df220c6340f4,c0084c81b975cebd8c69d20c1db282ec,913c8afc960193c6db4b0ebdb1d85e06,6fcc3c0d68bcbf5ea824af5019dd2177,60ebe8f518bc06a48f95c80aa9ba423e,efcf367e7abfcc1209426929813619b1,f5a6f1da2a39c678b30d794e83715d22,2c63dc92e6f0d5ebd4cf9221b2c0f45f,5496e882241e49079adf018aeedbe7e2,41b095d205527a3ddd468c065e663e13,ea92192db8bc850bf13f6201cb44abb4,c462dff9b138c9193a4ffa7dd37c19e4,ae911e39b4433e356139460a4830ec99,a1009d2cf123d3890ad85849f3a7ca5c,f3ebce6b41f431c88d948f7ffced7c4c,fe4684e329181a40a569f49e4340d8ed,1a7906dd093088c54d6ed3b5186b0cd5,2dc53280a0fc9d31202fc2e83e5e5985,f54f139b7d0802b2ef2ee21497161296,892bd5747cbe4ab4364085fae0122ec3,53bfadfb9624da2a08777cba4fa956c7,ab8d654bd2d1c788def8eff6c027cddc,d109f9717a454e04231cecaf160fb0a3" --quiet --output-document=/dev/null

