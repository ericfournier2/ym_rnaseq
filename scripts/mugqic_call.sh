export RAP_ID=def-crobert
module load mugqic/mugqic_pipelines


mkdir -p output/rna-pipeline-GRCh38
/home/efournie/genpipes/pipelines/rnaseq/rnaseq.py -j slurm -s '22' -l debug \
    -r raw/readset.txt \
    -d raw/design.txt \
    -o output/pipeline \
    --config /home/efournie/genpipes/pipelines/rnaseq/rnaseq.base.ini \
        /home/efournie/genpipes/pipelines/rnaseq/rnaseq.cedar.ini \
        ref/sus_scrofa.ini
